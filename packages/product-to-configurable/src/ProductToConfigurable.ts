import CommandsPluginInterface from '@m2dt/core/dist/plugin/CommandsPluginInterface'

export class ProductToConfigurable implements CommandsPluginInterface {
  getCommands () {
    return [
      // default command
      require('./commands/productToConfigurable.js').default,
      // sub-commands
      require('./commands/createOptionProducts.js').default.name('create-option-products'),
      require('./commands/findOptionsWithoutSKU.js').default.name('find-options-without-sku'),
      require('./commands/findProductsWithoutCost.js').default.name('find-products-without-cost'),
      require('./commands/fromConfig.js').default.name('from-config'),
      require('./commands/listSheets.js').default.name('list-sheets'),
      require('./commands/spreadsheetToConfig.js').default.name('spreadsheet-to-config'),
      require('./commands/spreadsheetToJSON.js').default.name('spreadsheet-to-json'),
      require('./commands/updateCustomisableOptionValueSKUs.js').default.name('update-customisable-option-value-skus'),
      require('./commands/shipping/estimateAll').default.name('shipping-estimate-all')
    ]
  }

  getNamespace () {
    return 'product-to-configurable'
  }
}

export default ProductToConfigurable
