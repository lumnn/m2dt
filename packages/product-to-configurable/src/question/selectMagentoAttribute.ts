import type { ListChoiceOptions as InquirerChoice } from 'inquirer'

interface SelectMagentoAttributeOptions {
  label?: string,
  allowNull?: boolean,
  nullLabel?: string,
  message?: string,
}

/**
 * Funtion that asks user to select attribute. It has an autocomplete functionality
 */
export async function selectMagentoAttribute(magento, options: SelectMagentoAttributeOptions = { allowNull: false, message: 'Choose Attribute' }) {
  const inquirer = require('inquirer')
  const chalk = require('chalk')
  const prompt = inquirer.createPromptModule()
  prompt.registerPrompt('autocomplete', require('inquirer-autocomplete-prompt'))

  let { attribute } = await prompt([
    {
      type: 'autocomplete',
      label: options.label || 'Choose Magento Attribute',
      name: 'attribute',
      message: options.message,
      source: async (prev, typed?: string) => {
        var filterGroups = [
          {
            filters: [
              {
                field: 'frontend_input',
                condition_type: 'in',
                value: 'select'
              },
              {
                field: 'is_global',
                condition_type: 'eq',
                value: 1
              },
            ]
          }
        ]

        if (typed) {
          filterGroups.push({
            filters: [
              {
                field: 'attribute_code',
                condition_type: 'like',
                value: `%${typed}%`
              },
              {
                field: 'attribute_id',
                condition_type: 'eq',
                value: typed
              }
            ]
          })
        }

        var result: any = await magento.get('products/attributes', {
          params: {
            fields: 'items[attribute_id,attribute_code,default_frontend_label,frontend_input,options]',
            searchCriteria: {
              pageSize: 10,
              filterGroups
            },
          }
        })

        var baseOptions: InquirerChoice[] = []

        if (options.allowNull) {
          baseOptions.unshift({
            name: options.nullLabel || '-- No Value --',
            value: null,
          })
        }

        if (!result.items) {
          return baseOptions
        }

        var choices = result.items
          .map((item, index) => ({
            name: (index + 1) + ' ' + (item.default_frontend_label || item.attribute_code) + ' ' + chalk.gray(`code: ${item.attribute_code}, options: ${item.options.length}`),
            value: item
          }))

        choices.unshift(...baseOptions)

        return choices
      }
    }
  ])

  return attribute
}
