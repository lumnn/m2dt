import XLSX from 'xlsx'
import inquirer from 'inquirer'
import inquirerAutocomplete from 'inquirer-autocomplete-prompt'

export async function chooseSheet(file: string): Promise<string> {
  inquirer.registerPrompt('autocomplete', inquirerAutocomplete)

  var workbook = XLSX.readFile(file, {
    bookSheets: true
  })

  const { sheetName } = await inquirer.prompt([{
    type: 'autocomplete',
    name: 'sheetName',
    message: 'Choose Sheet',
    source: async (prev, typed?: string) => {
      if (!typed) {
        return workbook.SheetNames
      }

      var lowerTyped = typed.toLowerCase().trim()

      return workbook.SheetNames
        .filter(sheetName => sheetName.toLowerCase().includes(lowerTyped))
        .sort((a, b) => a.toLowerCase().replace(lowerTyped, '').length - b.toLowerCase().replace(lowerTyped, '').length)
    },
  }])

  return sheetName
}
