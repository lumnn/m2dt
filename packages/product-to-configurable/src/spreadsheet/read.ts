import XLSX from 'xlsx'

export function read(file: string, sheetName): SheetProduct[] {
  // read only selected sheet
  const workbook = XLSX.readFile(file, {
    sheets: [sheetName],
    raw: true
  })

  const products: SheetProduct[] = []
  const sheet = workbook.Sheets[sheetName]
  const stringRange = sheet['!ref']

  if (typeof stringRange !== 'string') {
    throw new Error("Unexpected !ref output when reading sheet")
  }

  const sheetRange = XLSX.utils.decode_range(stringRange)

  // an object with column title as a key and it's index as value
  const columnRefs = {}
  const duplicatedColumns: any[] = []

  // we'll gather all headings here (into columnRefs, where we store column index)
  for (let col = sheetRange.s.c; col <= sheetRange.e.c; col++) {
    let cellRef = XLSX.utils.encode_cell({ c: col, r: 0 })

    if (!sheet[cellRef]) {
      throw new Error(`Field ${cellRef} is expected to have a title of contained data!`)
    }

    const title = sheet[cellRef].v
    let uniqueTitle = title
    let index = 1

    if (columnRefs[title] && !duplicatedColumns.includes(title)) {
      duplicatedColumns.push(title)
    }

    while (columnRefs[uniqueTitle] !== undefined) {
      uniqueTitle = title + '_' + index++
    }

    columnRefs[uniqueTitle] = col
  }

  var row = 1

  //
  // here we start with first loop. This one will find us products and their ranges
  //

  // loop until we reach the end of spreadsheet
  while (row <= sheetRange.e.r) {
    // it's intentional as there is inner loop modifying row value, therefore I want to catch this situation rather than stopping loop earlier
    if (row === sheetRange.e.r) {
      throw new Error(`Unexpected end of file. Probably it's because you have a product that has no option values?`)
    }

    // find sku cell
    const skuCellRef = XLSX.utils.encode_cell({
      c: columnRefs['Product Model/SKU'],
      r: row
    })
    const skuCell = sheet[skuCellRef]

    if (!skuCell) {
      throw new Error(`Cell ${skuCellRef} was expected to have product SKU`)
    }

    const sku: string = skuCell.v

    const costRef = XLSX.utils.encode_cell({ c: columnRefs['P. Cost Price'], r: row })
    const costCell = sheet[costRef]
    const cost = costCell ? costCell.v : undefined

    // name
    const nameRef = XLSX.utils.encode_cell({ c: columnRefs['Product Name'], r: row })
    const nameCell = sheet[nameRef]
    let name: string|undefined

    if (nameCell && nameCell.v.trim()) {
      name = nameCell.v.trim()
    }

    const currentProduct: SheetProduct = {
      sku,
      cost,
      name,
      rowStart: row,
      rowEnd: row,
      options: [],
      sheetName,
    }

    // find first option
    const firstOptionNameRef = XLSX.utils.encode_cell({ c: columnRefs['Options'], r: row })
    const firstOptionCell = sheet[firstOptionNameRef]

    if (firstOptionCell) {
      currentProduct.options.push({ name: firstOptionCell.v, values: [] })
    }

    // find additional options
    var additionalOptionIndex = 1
    while (true) {
      let additionalOptionNameRef = XLSX.utils.encode_cell({
        c: columnRefs[`Options_${additionalOptionIndex++}`],
        r: row
      })
      let additionalOptionNameCell = sheet[additionalOptionNameRef]

      if (!additionalOptionNameCell) {
        break
      }

      currentProduct.options.push({ name: additionalOptionNameCell.v, values: [] })
    }

    var nextRow = row

    do {
      nextRow++

      let nextRowSkuCellRef = XLSX.utils.encode_cell({
        c: columnRefs['Product Model/SKU'],
        r: nextRow
      })
      var nextRowSkuCell = sheet[nextRowSkuCellRef]

      // loop as long the next value is empty (same product)
      // or we reached end of the spreadsheet
    } while(!nextRowSkuCell && nextRow <= sheetRange.e.r)

    if (nextRow - row < 2) {
      throw new Error(`Product ${currentProduct.sku} seems to not have any options`)
    }

    // The next iteration should be from next product.
    // The rowEnd is reduced by one as it's value is referencing
    // next product row after do..while loop break
    row = nextRow
    currentProduct.rowEnd = nextRow - 1
    products.push(currentProduct)
  }

  //
  // second loop, to gather option values
  //

  for (var product of products) {
    var { rowStart, rowEnd } = product

    for (var index in product.options) {
      let option = product.options[index]

      for (let optionRow = rowStart + 1; optionRow <= rowEnd; optionRow++) {
        // the column suffix for option values
        const titleSuffix = index === '0' ? '' : ('_' + index)

        const valueNameRef = XLSX.utils.encode_cell({ r: optionRow, c: columnRefs[`Options${titleSuffix}`] })
        const valueNameCell = sheet[valueNameRef]

        const name: string|null = valueNameCell && valueNameCell.v
          ? valueNameCell.v.toString().trim()
          : null

        if (!name) {
          break
        }

        const valueTypeRef = XLSX.utils.encode_cell({ r: optionRow, c: columnRefs[`Option Type${titleSuffix}`] })
        const valueExistingRef = XLSX.utils.encode_cell({ r: optionRow, c: columnRefs[`Existing or New${titleSuffix}`] })
        const valueSkuRef = XLSX.utils.encode_cell({ r: optionRow, c: columnRefs[`Option SKU${titleSuffix}`] })
        const valueSupplierRef = XLSX.utils.encode_cell({ r: optionRow, c: columnRefs[`O. Supplier${titleSuffix}`] })
        const valueCostRef = XLSX.utils.encode_cell({ r: optionRow, c: columnRefs[`O. Cost Price${titleSuffix}`] })

        const valueTypeCell = sheet[valueTypeRef]
        const valueExistingCell = sheet[valueExistingRef]
        const valueSkuCell = sheet[valueSkuRef]
        const valueSupplierCell = sheet[valueSupplierRef]
        const valueCostCell = sheet[valueCostRef]

        let isConfigurableOption = valueTypeCell && valueTypeCell.v
          ? valueTypeCell.v.toLowerCase().trim() === 'product variation'
          : null

        if (isConfigurableOption === null) {
          console.log(`Expected option type in cell ${valueTypeRef} - Assuming customisable option`)
          isConfigurableOption = false
        }

        const sku = (valueSkuCell && valueSkuCell.v.toString().trim()) || null

        let priceOnApplication = false
        let cost = (valueCostCell ? valueCostCell.v : 0)

        if (cost === 'POA') {
          priceOnApplication = true
          cost = 0
        } else if (cost != 0 && isConfigurableOption) {
          // if cost is not 0, then it is probably cost for product with that option
          if (product.cost) {
            cost -= product.cost
            cost = Math.round((cost + Number.EPSILON) * 100) / 100
          }
        }

        const value: SheetProductOptionValue = {
          name,
          isConfigurableOption,
          sku,
          cost,
          priceOnApplication,
          exists: valueExistingCell ? valueExistingCell.v.toLowerCase().trim() === 'existing' : undefined
        }

        if (valueSupplierCell && valueSupplierCell.v.trim()) {
          value.supplier = valueSupplierCell.v.trim()
        }

        option.values.push(value)
      }

      let configurableOptions = option.values.filter(value => value.isConfigurableOption)

      if (configurableOptions.length !== 0 && configurableOptions.length !== option.values.length) {
        throw new Error(`${product.sku} All option values for "${option.name}" must have same type.`)
      }
    }
  }

  return products
}

interface SheetProduct {
  sku: string,
  name?: string,
  cost: number|null,
  sheetName: string,
  rowStart: number,
  rowEnd: number,
  options: SheetProductOption[],
  magentoProduct?: any,
}

interface SheetProductOption {
  name: string,
  values: SheetProductOptionValue[]
}

interface SheetProductOptionValue {
  name: string,
  sku: string|null,
  cost: number,
  priceOnApplication: boolean,
  isConfigurableOption: boolean,
  exists?: boolean,
  supplier?: string
}
