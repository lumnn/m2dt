import { M2DTCommand } from '@m2dt/core/dist/commander/M2DTCommand'
import errorParseMessage from '@m2dt/core/dist/magento/error/parseMessage'
import axios from 'axios'

const program = new M2DTCommand()

program
  .description("Check shipping for a product")
  .option('-p, --postcode <postcode>', 'Delivery Postcode. For example: DN22 7WF')
  .option('-C, --country <country>', 'Delivery Country. For example: GB')
  .option('-r, --region <region>', 'Delivery Region. For example: Nottinghamshire')
  .option('--output-attribute <attribute_code>', 'Add extra attribute to output')
  .action(async (program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()
    const address: any = {}

    if (program.region) {
      address.region = program.region
    }

    if (program.country) {
      address.country_id = program.country
    }

    if (program.postcode) {
      address.postcode = program.postcode
    }

    const cartId = await magento.post('/guest-carts')
    const limit = 10
    let currentPage = 1
    var totalCount: Number = 0
    var processed = 0

    var result: any[] = []

    do {
      var products = await magento.get(`products`, {
        params: {
          fields: 'total_count,items[sku,options,extension_attributes,custom_attributes]',
          searchCriteria: {
            currentPage: currentPage++,
            pageSize: limit,
            filterGroups: [
              {
                filters: [
                  {
                    condition_type: 'eq',
                    field: 'status',
                    value: 1
                  }
                ]
              },
              {
                filters: [
                  {
                    condition_type: 'eq',
                    field: 'visibility',
                    value: 4
                  }
                ]
              }
            ]
          }
        }
      })

      if (totalCount === 0) {
        totalCount = products.total_count
      }

      productLoop: for (const product of products.items) {
        const outputAttribute = product.custom_attributes.find(attr => attr.attribute_code === program.outputAttribute)
        const outputAttributeValue = outputAttribute ? outputAttribute.value : null

        const customOptions: any = []
        for (var option of product.options) {
          if (!option.is_require) {
            continue
          }

          customOptions.push({
            option_id: option.option_id,
            option_value: option.values[0].option_type_id
          })
        }

        const configurableItemOptions: any = []
        if (product.extension_attributes.configurable_product_options) {
          for (var option of product.extension_attributes.configurable_product_options) {
            if (option.values.length === 0) {
              process.stderr.write(`${product.sku} - One of the option doesn't have values\n`)

              continue productLoop
            }

            configurableItemOptions.push({
              option_id: option.attribute_id,
              option_value: option.values[0].value_index
            })
          }
        }

        try {
          var addedItem = await magento.post(`/guest-carts/${cartId}/items`, {
            cartItem: {
              sku: product.sku,
              qty: 1,
              quote_id: cartId,
              product_option: {
                extension_attributes: {
                  custom_options: customOptions,
                  configurable_item_options: configurableItemOptions
                }
              }
            }
          })
        } catch (e) {
          if (!axios.isAxiosError(e) || !e.response) {
            throw e
          }

          if (e.response.status < 400 && e.response.status >= 500) {
            throw e
          }

          var errorMessage: any;

          try {
            errorMessage = errorParseMessage(e.response.data)
          } catch (e2) {
            console.log(e2)
          }

          if (!errorMessage && e.response.data) {
            errorMessage = JSON.stringify(e.response.data)
          }

          process.stderr.write(`${product.sku} - Error during checking shipping. ${errorMessage}\n`)

          continue
        }

        const estimation = await magento.post(`/guest-carts/${cartId}/estimate-shipping-methods`, {
          address
        })

        await magento.delete(`/guest-carts/${cartId}/items/${addedItem.item_id}`)

        estimation.sort((a, b) => a.amount - b.amount)

        const cheapest = estimation[0] ? estimation[0].price_excl_tax : null

        process.stdout.write(`"${product.sku}"\t${cheapest}\t"${outputAttributeValue}"\n`)
      }

      processed += products.items.length
    } while (processed < totalCount)
  })

export default program
