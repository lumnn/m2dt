import chalk from 'chalk'
import { M2DTCommand } from '@m2dt/core/dist/commander/M2DTCommand'
import inquirer from 'inquirer'
import { addOptionAction } from '@m2dt/core/dist/commands/attribute/addOption'
import { Attribute, isAttribute } from '@m2dt/core/dist/magento/types/Attribute'
import { AttributeOption, isAttributeOption } from '@m2dt/core/dist/magento/types/AttributeOption'
import { ProductOption, isProductOption } from '@m2dt/core/dist/magento/types/ProductOption'
import { ProductOptionValue, isProductOptionValue } from '@m2dt/core/dist/magento/types/ProductOptionValue'
import parseMessage from '@m2dt/core/dist/magento/error/parseMessage'
import axios from 'axios'

const program = new M2DTCommand()

export default program
  .arguments('<optionsConfigPath>')
  .description('Converts product into configurable based on previously created configuration')
  .option('--allow-missing-options', "Whether script should continue, where mapped original options are missing?")
  .action(async (optionsConfigPath: string, program) => {
    if (optionsConfigPath[0] !== '/') {
      const path = require('path')
      optionsConfigPath = path.join(process.cwd(), optionsConfigPath)
    }

    var optionsConfig = require(optionsConfigPath)

    if (!optionsConfig.sku) {
      throw new Error("Missing product SKU in optionsConfig")
    }

    return await fromConfigAction(optionsConfig.sku, optionsConfig, program)
  })

async function fromConfigAction(productSku: string, config, program: M2DTCommand) {
  const magento = await program.getMagentoInstance()
  const product = await magento.get(`products/${encodeURIComponent(productSku)}`)
  const attributeSetId = product.attribute_set_id
  const attributeSetAttributes = await magento.get(`products/attribute-sets/${attributeSetId}/attributes`)
  const allowMissingOptions = program.allowMissingOptions

  console.log(`${chalk.cyan('Product:')} ${product.sku} - ${product.name}`)

  if (!isProductConversionConfig(config)) {
    throw new Error("config is not instanceof ProductConversionConfig interface")
  }

  console.log(`Checking whether all attributes exist with necessary options...`)

  for (const optionConfig of config.configurableOptions) {
    var attribute = optionConfig.attribute

    try {
      var existingAttribute = await magento.get(`products/attributes/${attribute.attribute_code}`)
    } catch(e) {
      if (!axios.isAxiosError(e)) {
        throw e
      }

      if (e.response && e.response.status === 404) {
        console.log(`  ${attribute.attribute_code} - Doesn't exist. Creating...`)

        var options = attribute.options
          ? attribute.options.map(option => ({ label: option.label })).filter(option => option.label.trim() !== '')
          : undefined

        existingAttribute = await magento.put(`products/attributes/${attribute.attribute_code}`, {
          attribute: {
            scope: attribute.scope,
            frontend_input: attribute.frontend_input,
            default_frontend_label: attribute.default_frontend_label,
            is_required: attribute.is_required,
            options
          }
        })
      }
    }

    var valuesMapping = optionConfig.valuesMapping

    for (const mapping of valuesMapping) {
      var attributeOption = mapping.attributeOption

      var existingOption = existingAttribute.options.find(existingOption => existingOption.label.toLowerCase() === attributeOption.label.toLowerCase())

      if (!existingOption) {
        console.log(`    ${attributeOption.label} - Option doesn't exist. Adding...`)
        existingOption = await addOptionAction(existingAttribute.attribute_code, attributeOption.label, program)
      }

      attributeOption.value = existingOption.value
    }

    optionConfig.attribute = existingAttribute
  }

  var missingSetAttributes: string[] = []

  for (var optionConfig of config.configurableOptions) {
    console.log()
    var option: any = null

    if (optionConfig.option) {
      var mageOption = optionConfig.option
      option = product.options.find(option => option.option_id === mageOption.option_id)

      if (option) {
        console.log(`${chalk.red('Option will be removed:')} #${option.option_id} - ${option.title}`)

        if (!option.is_require) {
          console.log(chalk.bgRed("That option is currently not required!"))
        }
      } else {
        if (!allowMissingOptions) {
          throw new Error(`Couldn't find option with id #${optionConfig.option.option_id} on product`)
        }

        console.log(`Missing option: ${optionConfig.option.title}`)
      }
    }

    console.log(`${chalk.green('Configurable Option will be added:')} code: ${optionConfig.attribute.attribute_code}`)

    var magentoAttribute = attributeSetAttributes.find(attribute => attribute.attribute_code === optionConfig.attribute.attribute_code)

    if (!magentoAttribute) {
      const attributeSet = await magento.get(`products/attribute-sets/${attributeSetId}`)
      console.log(chalk.bgRed(`Attribute "${optionConfig.attribute.attribute_code}" is not in the Attribute Set "${attributeSet.attribute_set_name}" used by this product!`))
      missingSetAttributes.push(optionConfig.attribute.attribute_code)
    }

    if (!option) {
      continue
    }

    optionConfig.valuesMapping.forEach(mapping => {
      var price = mapping.price || mapping.optionValue.price
      var optionPriceSku = chalk.gray(`sku: ${mapping.sku}, price: ${price}`)

      if (mapping.optionValue) {
        var optionValue = option.values.find(value => value.option_type_id === mapping.optionValue.option_type_id)

        if (!optionValue) {
          throw new Error(`Could not find option value with ${mapping.optionValue.option_type_id} in product`)
        }

        console.log(` - ${chalk.gray(optionValue.title)} ${chalk.green('=>')} (${mapping.attributeOption.value}) ${mapping.attributeOption.label} - ${optionPriceSku}`)
        return
      }

      console.log(` - ${mapping.attributeOption.label} - ${optionPriceSku}`)
    })
  }

  if (missingSetAttributes.length > 0) {
    console.log()
    console.log(`Assigning attributes to set: ${missingSetAttributes.join(',')}`)
    await assignAttributesToGroup(magento, product.attribute_set_id, missingSetAttributes)
    console.log("Added!")
  }

  console.log()
  console.log(chalk.bgGreen("New Product variants"))

  if (product.price === 0) {
    const { price } = await inquirer.prompt({
      type: 'number',
      name: 'price',
      message: 'Enter base price for product (API returned 0). Default value is what we had in a config',
      default: config.price
    })
    product.price = price
  }

  var variants = [{
    name: product.name,
    sku: product.sku,
    price: +product.price,
    custom_attributes: {}
  }]

  config.configurableOptions.forEach(option => {
    var variantsBase = variants
    variants = []

    option.valuesMapping.forEach(value => {
      var newOptionValue = {
        ...mergeVariantAttributes(config.attributes, value.attributes),
        [option.attribute.attribute_code]: value.attributeOption.value
      }

      var valuePrice: number

      if (value.price) {
        valuePrice = value.price
      } else if (value.optionValue && value.optionValue.price) {
        valuePrice = value.optionValue.price
      } else {
        valuePrice = 0
      }

      variants.push(...variantsBase.map((variant, index) => {
        return {
          name: variant.name + ' - ' + value.attributeOption.label,
          sku: variant.sku + (value.sku || index),
          price: variant.price + valuePrice,
          custom_attributes: {
            ...variant.custom_attributes,
            ...newOptionValue
          }
        }
      }))
    })
  })

  var stockItem = Object.assign({}, product.extension_attributes.stock_item)
  delete stockItem.item_id
  delete stockItem.product_id
  delete stockItem.stock_id

  var nonConfigurableAttributes = product.custom_attributes
    .filter(attr => {
      // check whether the attribute exists in configurableOptions mapping in config
      // if yes, then we don't add it to variant prototype
      return !config.configurableOptions.find(optionMapping => optionMapping.attribute.attribute_code === attr.attribute_code)
    })

  variants = variants.map(variant => {
    var variantAttributes = nonConfigurableAttributes.map(attr => ({ ...attr }))

    for (let attributeCode in variant.custom_attributes) {
      variantAttributes.push({
        attribute_code: attributeCode,
        value: variant.custom_attributes[attributeCode]
      })
    }

    var sku = variant.sku.replace(/(.*<<)/, '')

    if (sku !== variant.sku) {
      console.log(chalk.yellow(`Updating SKU from ${variant.sku} to ${sku}`))
    }

    // custom kingfisher one, to set new merchant feed ID
    var merchantFeedId = variantAttributes.find(attr => attr.attribute_code === 'merchant_feed_product_id')
    if (merchantFeedId) {
      merchantFeedId.value = sku
    }
    // end-custom

    return {
      ...variant,
      sku,
      status: product.status,
      attribute_set_id: product.attribute_set_id,
      visibility: 1,
      type_id: 'simple',
      extension_attributes: {
        website_ids: product.extension_attributes.website_ids,
        stock_item: {...stockItem},
      },
      custom_attributes: variantAttributes
    }
  })

  var savedVariantIDs: string[] = []

  console.log("Creating variants in magento...")

  for (const product of variants) {
    console.log(`  Creating product ${product.sku}`)

    try {
      var existing = await magento.get(`products/${encodeURIComponent(product.sku)}`)

      console.log(`${existing.sku} - ${existing.name}`)
      let prompt = await inquirer.prompt([{ type: 'confirm', name: 'continue', message: 'Product with this SKU already exists. Are you sure you want to continue?' }])

      if (!prompt.continue) {
        throw new Error(`Product with SKU ${existing.sku}, named: ${existing.name} already exists!`)
      }
    } catch (e) {
      if (!axios.isAxiosError(e) || (e.response && e.response.status !== 404)) {
        throw e
      }
    }

    try {
      var response = await magento.post('products/', {
        product
      })
    } catch (e) {
      console.log(product)
      throw e
    }

    savedVariantIDs.push(response.id)
  }

  console.log(`Updating main product to be configurable...`)

  await magento.put(`products/${encodeURIComponent(product.sku)}`, {
    product: {
      type_id: 'configurable'
    }
  })

  console.log(`Removing existing product opions...`)

  for (const optionMapping of config.configurableOptions) {
    const mageOption = optionMapping.option

    if (!mageOption) {
      continue
    }

    try {
      await magento.delete(`products/${encodeURIComponent(product.sku)}/options/${mageOption.option_id}`)
    } catch (e) {
      if (axios.isAxiosError(e) && e.response && e.response.status === 404) {
        console.log(chalk.yellow(`  ${mageOption.title} - Already missing`))
        continue
      }

      throw e
    }

    console.log(`  ${mageOption.title} - OK`)
  }

  console.log(`Specifying configurable attributes on base product...`)

  for (const optionMapping of config.configurableOptions) {
    const mageOption = optionMapping.option
    const mageAttribute = optionMapping.attribute

    const mageOptionValues = optionMapping.valuesMapping.map(mapping => {
      return {
        value_index: mapping.attributeOption.value
      }
    })

    const option: any = {
      attribute_id: mageAttribute.attribute_id,
      values: mageOptionValues,
    }

    if (mageOption) {
      option.label = mageOption.title
      option.position = mageOption.sort_order
    }

    await magento.post(`configurable-products/${encodeURIComponent(product.sku)}/options`, {
      option
    })

    console.log(`  ${mageAttribute.attribute_code} - OK`)
  }

  console.log(`Linking variants to configurable products...`)

  for (const variant of variants) {
    var error

    do {
      error = null

      try {
        await magento.post(`configurable-products/${encodeURIComponent(product.sku)}/child`, {
          childSku: variant.sku,
        })
      } catch (e) {
        if (!axios.isAxiosError(e)) {
          throw e
        }

        if (e.response && e.response.data && e.response.data.message === "The product is already attached.") {
          console.log(`  ${variant.sku} - Already attached`)
          continue
        }

        if (e.response && e.response.data && e.response.data.message) {
          console.log(`  ${variant.sku} - ${parseMessage(e.response.data)}`)

          let { again } = await inquirer.prompt([{ type: 'confirm', name: 'again', message: 'Do you want to try again?' }])

          if (again) {
            error = e
            continue
          }
        }

        throw e
      }
    } while (error)

    console.log(`  ${variant.sku} - OK`)
  }

  console.log("Should be done. Should...")
}

async function assignAttributesToGroup(magento: any, attributeSetId: number, attributeCodes: string[]) {
  var groups = await magento.get('products/attribute-sets/groups/list', {
    params: {
      fields: "items,total_count",
      searchCriteria: {
        pageSize: 1,
        currentPage: 1,
        filterGroups: [{
          filters: [
            {
              field: 'attribute_group_name',
              condition_type: 'eq',
              value: 'Attributes'
            },
            {
              field: 'attribute_set_id',
              condition_type: 'eq',
              value: attributeSetId
            },
          ]
        }]
      }
    }
  })

  if (groups.total_count !== 1) {
    throw new Error(`Found ${groups.total_count} matching attribute set groups. Expected 1`)
  }

  var attributeGroupId = groups.items[0].attribute_group_id

  for (const attributeCode of attributeCodes) {
    await magento.post(
      'products/attribute-sets/attributes',
      {
        attributeSetId,
        attributeGroupId,
        attributeCode,
        sortOrder: 100
      }
    )
  }
}

export interface ProductConversionConfig {
  // sku of converted product
  sku: string,
  // mapping configuration between current options an variants to be created
  configurableOptions: OptionConversionConfig[],
  // product price, although the magento api will be treated as a source of this value
  // after conversion into configurable product this information is beeing lost on Magento
  price: number,
  // extra product attributes to add when creating/updating product
  attributes: ProductConversionAttributes,
  // i forgot why :)
  options?: any[]
}

interface ProductConversionAttributes {
  [name: string]: string|number|boolean|null
}

export function isProductConversionConfig(arg: any): arg is ProductConversionConfig {
  if (
    typeof arg === 'object'
    && typeof arg.sku === 'string'
    && Array.isArray(arg.configurableOptions)
    && arg.configurableOptions.every(isOptionConversionConfig)
    && (arg.options === undefined || Array.isArray(arg.options))
  ) {
    return true
  }

  return false
}

export interface OptionConversionConfig {
  // magento option, null to create new configurable option
  option: ProductOption|null,
  // magento attribute
  attribute: Attribute,
  // mappings between options and attribute values
  valuesMapping: OptionValueMapping[]
}

export function isOptionConversionConfig(arg: any): arg is OptionConversionConfig {
  if (
    typeof arg === 'object'
    && isProductOption(arg.option)
    && isAttribute(arg.attribute)
    && Array.isArray(arg.valuesMapping)
    && arg.valuesMapping.every(isOptionValueMapping)
  ) {
    return true
  }

  return false
}

export interface OptionValueMapping {
  // magento option value
  optionValue: ProductOptionValue,
  // magento attribute value
  attributeOption: AttributeOption,
  // sku to append to product
  sku: string,
  // attributes to modify on variant
  attributes: ProductConversionAttributes,
  // price to modify on variant
  price?: number,
}

export function isOptionValueMapping(arg: any): arg is OptionValueMapping {
  if (
    typeof arg === 'object'
    && isProductOptionValue(arg.optionValue)
    && isAttributeOption(arg.attributeOption)
    && typeof arg.sku === 'string'
    && ['undefined', 'number'].includes(typeof arg.price)
    && typeof arg.attributes === 'object'
  ) {
    return true
  }

  return false
}

function mergeVariantAttributes(a: ProductConversionAttributes, b: ProductConversionAttributes): ProductConversionAttributes {
  var attributes: ProductConversionAttributes = {}

  if (a.price_on_application || b.price_on_application) {
    attributes.price_on_application = true
  } else if (typeof a.cost === 'number' || typeof b.cost === 'number') {
    var aCost = typeof a.cost === 'number' ? a.cost : 0
    var bCost = typeof b.cost === 'number' ? b.cost : 0

    attributes.cost = aCost + bCost
  }

  return attributes
}

export { fromConfigAction, program as fromConfig }
