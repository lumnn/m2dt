import { M2DTCommand } from '@m2dt/core/dist/commander/M2DTCommand'
import path from 'path'
import fs from 'fs'
import XLSX from 'xlsx'
import inquirer from 'inquirer'
import chalk from 'chalk'
import { ProductConversionConfig, OptionConversionConfig } from './fromConfig'
import { ProductOption as MageProductOption, isProductOption } from '@m2dt/core/dist/magento/types/ProductOption'
import { selectMagentoAttribute } from '../question/selectMagentoAttribute'
import { createAttributeForConfigurable } from '@m2dt/core/dist/commands/attribute/createForConfigurable'
import inquirerAutocomplete from 'inquirer-autocomplete-prompt'
import { chooseSheet } from '../spreadsheet/chooseSheet'

inquirer.registerPrompt('autocomplete', inquirerAutocomplete)

const program = new M2DTCommand()

export default program
  .arguments('<file>')
  .option('--save-folder <folder>', 'File where JSON output should be saved')
  .action(async (file: string, program) => {
    if (file[0] !== '/') {
      file = path.join(process.cwd(), file)
    }

    if (!fs.existsSync(file)) {
      throw new Error(`Couldn't find file ${file}`)
    }

    const sheetName = await chooseSheet(file)

    // read only selected sheet
    var workbook = XLSX.readFile(file, {
      sheets: [sheetName],
      raw: true
    })

    var sheet = workbook.Sheets[sheetName]
    var stringRange = sheet['!ref']

    if (typeof stringRange !== 'string') {
      throw new Error("Unexpected !ref output when reading sheet")
    }

    var sheetRange = XLSX.utils.decode_range(stringRange)

    // an object with column title as a key and it's index as value
    var columnRefs = {}
    var duplicatedColumns: any[] = []

    // we'll gather all headings here (into columnRefs, where we store column index)
    for (let col = sheetRange.s.c; col <= sheetRange.e.c; col++) {
      let cellRef = XLSX.utils.encode_cell({ c: col, r: 0 })

      if (!sheet[cellRef]) {
        throw new Error(`Field ${cellRef} is expected to have a title of contained data!`)
      }

      const title = sheet[cellRef].v
      let uniqueTitle = title
      let index = 1

      if (columnRefs[title] && !duplicatedColumns.includes(title)) {
        duplicatedColumns.push(title)
      }

      while (columnRefs[uniqueTitle] !== undefined) {
        uniqueTitle = title + '_' + index++
      }

      columnRefs[uniqueTitle] = col
    }

    var row = 1
    var products: SheetProduct[] = []

    //
    // here we start with first loop. This one will find us products and their ranges
    //

    // loop until we reach the end of spreadsheet
    while (row <= sheetRange.e.r) {
      // it's intentional as there is inner loop modifying row value, therefore I want to catch this situation rather than stopping loop earlier
      if (row === sheetRange.e.r) {
        throw new Error(`Unexpected end of file. Probably it's because you have a product that has no option values?`)
      }

      // find sku cell
      const skuCellRef = XLSX.utils.encode_cell({
        c: columnRefs['Product Model/SKU'],
        r: row
      })
      const skuCell = sheet[skuCellRef]

      if (!skuCell) {
        throw new Error(`Cell ${skuCellRef} was expected to have product SKU`)
      }

      const sku: string = skuCell.v.toString()

      const costRef = XLSX.utils.encode_cell({ c: columnRefs['P. Cost Price'], r: row })
      const costCell = sheet[costRef]
      const cost = costCell ? costCell.v : undefined

      // name
      const nameRef = XLSX.utils.encode_cell({ c: columnRefs['Product Name'], r: row })
      const nameCell = sheet[nameRef]
      let name: string|undefined

      if (nameCell && nameCell.v.trim()) {
        name = nameCell.v.trim()
      }

      const currentProduct: SheetProduct = {
        sku,
        cost,
        name,
        rowStart: row,
        rowEnd: row,
        options: [],
      }

      // find first option
      const firstOptionNameRef = XLSX.utils.encode_cell({ c: columnRefs['Options'], r: row })
      const firstOptionCell = sheet[firstOptionNameRef]

      if (firstOptionCell) {
        currentProduct.options.push({ name: firstOptionCell.v, values: [] })
      }

      // find additional options
      var additionalOptionIndex = 1
      while (true) {
        let additionalOptionNameRef = XLSX.utils.encode_cell({
          c: columnRefs[`Options_${additionalOptionIndex++}`],
          r: row
        })
        let additionalOptionNameCell = sheet[additionalOptionNameRef]

        if (!additionalOptionNameCell) {
          break
        }

        currentProduct.options.push({ name: additionalOptionNameCell.v, values: [] })
      }

      var nextRow = row

      do {
        nextRow++

        let nextRowSkuCellRef = XLSX.utils.encode_cell({
          c: columnRefs['Product Model/SKU'],
          r: nextRow
        })
        var nextRowSkuCell = sheet[nextRowSkuCellRef]

        // loop as long the next value is empty (same product)
        // or we reached end of the spreadsheet
      } while(!nextRowSkuCell && nextRow <= sheetRange.e.r)

      if (nextRow - row < 2) {
        throw new Error(`Product ${currentProduct.sku} seems to not have any options`)
      }

      // The next iteration should be from next product.
      // The rowEnd is reduced by one as it's value is referencing
      // next product row after do..while loop break
      row = nextRow
      currentProduct.rowEnd = nextRow - 1
      products.push(currentProduct)
    }

    //
    // second loop, to gather option values
    //

    for (var product of products) {
      var { rowStart, rowEnd } = product

      for (var index in product.options) {
        let option = product.options[index]

        for (let optionRow = rowStart + 1; optionRow <= rowEnd; optionRow++) {
          // the column suffix for option values
          const titleSuffix = index === '0' ? '' : ('_' + index)

          const valueNameRef = XLSX.utils.encode_cell({ r: optionRow, c: columnRefs[`Options${titleSuffix}`] })
          const valueNameCell = sheet[valueNameRef]

          const name: string|null = valueNameCell && valueNameCell.v
            ? valueNameCell.v.toString().trim()
            : null

          if (!name) {
            break
          }

          const valueTypeRef = XLSX.utils.encode_cell({ r: optionRow, c: columnRefs[`Option Type${titleSuffix}`] })
          const valueExistingRef = XLSX.utils.encode_cell({ r: optionRow, c: columnRefs[`Existing or New${titleSuffix}`] })
          const valueSkuRef = XLSX.utils.encode_cell({ r: optionRow, c: columnRefs[`Option SKU${titleSuffix}`] })
          const valueSupplierRef = XLSX.utils.encode_cell({ r: optionRow, c: columnRefs[`O. Supplier${titleSuffix}`] })
          const valueCostRef = XLSX.utils.encode_cell({ r: optionRow, c: columnRefs[`O. Cost Price${titleSuffix}`] })

          const valueTypeCell = sheet[valueTypeRef]
          const valueExistingCell = sheet[valueExistingRef]
          const valueSkuCell = sheet[valueSkuRef]
          const valueSupplierCell = sheet[valueSupplierRef]
          const valueCostCell = sheet[valueCostRef]

          let isConfigurableOption = valueTypeCell && valueTypeCell.v
            ? valueTypeCell.v.toLowerCase().trim() === 'product variation'
            : null

          if (isConfigurableOption === null) {
            console.log(chalk.yellow(`Expected option type in cell ${valueTypeRef} - Assuming customisable option`))
            isConfigurableOption = false
          }

          const sku = (valueSkuCell && valueSkuCell.v.toString().trim()) || null

          let priceOnApplication = false
          let cost = (valueCostCell ? valueCostCell.v : 0)

          if (cost === 'POA') {
            priceOnApplication = true
            cost = 0
          } else if (cost != 0) {
            // if cost is not 0, then it is probably cost for product with that option
            if (product.cost) {
              cost -= product.cost
              cost = Math.round((cost + Number.EPSILON) * 100) / 100
            }
          }

          const value: SheetProductOptionValue = {
            name,
            isConfigurableOption,
            sku,
            cost,
            priceOnApplication,
            exists: valueExistingCell ? valueExistingCell.v.toLowerCase().trim() === 'existing' : undefined
          }

          if (valueSupplierCell && valueSupplierCell.v.trim()) {
            value.supplier = valueSupplierCell.v.trim()
          }

          option.values.push(value)
        }

        let configurableOptions = option.values.filter(value => value.isConfigurableOption)

        if (configurableOptions.length !== 0 && configurableOptions.length !== option.values.length) {
          throw new Error(`${product.sku} All option values for "${option.name}" must have same type.`)
        }
      }
    }

    //
    // another loop to ensure that we have all products in magento
    //

    var magento = await program.parent.getMagentoInstance()
    const magentoProductsResponse = await magento.get('products', {
      params: {
        searchCriteria: {
          filterGroups: [{
            filters: [{
              field: 'sku',
              condition_type: 'in',
              value: products.map(product => product.sku).join(',')
            }]
          }]
        }
      }
    })
    const magentoProducts = magentoProductsResponse.items

    for (var product of products) {
      console.log(product.sku)
      console.log(product.sku.toString())
      const magentoProduct = magentoProducts.find(mageProduct => mageProduct.sku === product.sku)
      if (!magentoProduct) {
        console.log(chalk.red(`Missing magento product with sku ${product.sku}`))
        continue;
      }

      product.magentoProduct = magentoProduct

      process.env.VERBOSE && console.log(chalk.gray(`${product.sku}: Product exists`))

      if (product.name && magentoProduct.name.toLowerCase().trim() !== product.name.toLowerCase().trim()) {
        console.log(chalk.red(`${product.sku}: Name from spreadsheet doesn't match magento name "${magentoProduct.name}"`))
      }

      for (var productOption of product.options) {
        var mageOption = magentoProduct.options.find(mageOption => mageOption.title.toLowerCase().trim() === productOption.name.toLowerCase().trim())

        if (!mageOption) {
          console.log(chalk.red(`${product.sku}: Couldn't find magento option with name "${productOption.name}"`))
          continue
        }

        process.env.VERBOSE && console.log(chalk.gray(`${product.sku}: Option "${mageOption.title}" exists`))
        productOption.magentoOption = mageOption

        for (var optionValue of productOption.values) {
          var mageOptionValue = mageOption.values.find(mageValue => mageValue.title.toLowerCase().trim() === optionValue.name.toLowerCase().trim())

          if (!mageOptionValue) {
            console.log(chalk.red(`${product.sku}: Couldn't find magento option value "${optionValue.name}" for option "${productOption.name}"`))
            continue
          }

          process.env.VERBOSE && console.log(chalk.gray(`${product.sku}: Option value "${mageOptionValue.title}" exists`))
          optionValue.magentoOptionValueId = mageOptionValue.option_type_id
        }
      }
    }

    //
    // Now we shoul know the state of magento products.
    //
    // We proceed to grouping same-title options (which ideally should be converted into single attribute)
    //

    var optionsByTitle = {}

    for (var product of products) {
      for (var option of product.options) {
        if (!option.values.find(value => value.isConfigurableOption)) {
          // no option value marked as configurable ("product variant")
          continue
        }

        const title = option.name.toLowerCase().trim()

        if (!optionsByTitle[title]) {
          optionsByTitle[title] = [option]
          continue
        }

        optionsByTitle[title].push(option)
      }
    }

    var attributesByOptionTitle = {}

    for (let title in optionsByTitle) {
      let options = optionsByTitle[title]
      options.forEach(option => console.log(option.name))

      let magentoAttribute = await selectMagentoAttribute(magento, {
        allowNull: true,
        nullLabel: "-- None is matching, create new one --",
        label: `Select attribute to use for above option`
      })

      if (!magentoAttribute) {
        console.log(chalk.yellow("Please provide neccessary values for new attribute"))
        magentoAttribute = await createAttributeForConfigurable(magento, null, {
          addOptions: false,
          defaults: {
            attribute_code: title.replace(/[^a-zA-Z0-9]+/g, '_'),
            default_frontend_label: options[0].name,
          }
        })
      }

      attributesByOptionTitle[title] = magentoAttribute
    }

    var conversionConfig: ProductConversionConfig[] = []

    productLoop: for (var product of products) {
      let configurableOptions: OptionConversionConfig[] = []

      if (!product.magentoProduct) {
        console.log(chalk.red(`Product ${product.sku} is ignored from output as Magento data is missing`))
        continue
      }

      for (let option of product.options) {
        if (option.values.length < 1) {
          throw new Error(`Product: ${product.sku}, Option: ${option.name} - Missing Option Values`)
        }

        // we don't want non-configurable options here
        if (!option.values[0].isConfigurableOption) {
          continue
        }

        if (!option.magentoOption) {
          console.error(
            `Product: ${product.sku}, Option: ${option.name} - ` +
            "Cannot generate conversion config without having magentoOption specified on SheetProductOption. This is probably because Magento Product Option wasn't found in product with same title. It will be ignored in output"
          )

          continue productLoop
        }

        if (!option.magentoOption.values){
          throw new Error(
            `Product: ${product.sku}, Option: ${option.name} -` +
            "Cannot proceed without having SheetProductOption.magentoOption.values. This should not happen, the exception is here to make TypeScript happy. However, if it reaches this point there is something wrong with script or Magento API"
          )
        }

        var magentoOptionValues = option.magentoOption.values

        var attribute = attributesByOptionTitle[option.name.toLowerCase().trim()]

        if (!attribute) {
          throw new Error(`For some reason attribute code for option "${option.name}" was not found`)
        }

        const existingValueSKUs: string[] = [product.sku]

        configurableOptions.push({
          option: {
            option_id: option.magentoOption.option_id,
            title: option.magentoOption.title,
          },
          attribute,
          valuesMapping: option.values.map((optionValue, index) => {
              if (!optionValue.magentoOptionValueId) {
                throw new Error(`\
                  Product: ${product.sku}, Option: ${option.name}, Option Value: ${optionValue.name} -\
                  Cannot proceed without having all magento product options found in Magento
                `)
              }

              // attempt to find the ProductOptionValue
              var magentoOptionValue = magentoOptionValues.find(magentoOptionValue => {
                return magentoOptionValue.option_type_id === optionValue.magentoOptionValueId
              })

              if (!magentoOptionValue) {
                throw new Error(
                  `Product: ${product.sku}, Option: ${option.name}, Option Value: ${optionValue.name} -` +
                  "Option Value was not found even though the ID was earlier set. This should not happen in this script as we earlier set this value based on available option. However, if it happes, it means that either there is something wrong in this script"
                )
              }

              var attributeOptionLabel = optionValue.name

              if (optionValue.sku) {
                var skuRegex = new RegExp('\\(' + optionValue.sku.replace(/[.*+?^${}()|[\]\\]/g, '\\$&') + '\\)', 'g')
                attributeOptionLabel = attributeOptionLabel.replace(skuRegex, '').trim()
              }

              var sku = optionValue.sku || '-' + index.toString()

              var skuDuplicateIndex = 1
              // check whether the sku is already used (it removes the << prefix if needed)
              while (existingValueSKUs.includes(sku.replace(/^<</, ''))) {
                var newSku = optionValue.sku
                  ? (optionValue.sku + '-' + skuDuplicateIndex++)
                  : "-" + (index + skuDuplicateIndex++).toString()

                sku = newSku
              }

              if (sku !== optionValue.sku) {
                console.log(chalk.yellow(
                  `Product: ${product.sku}, Option: ${option.name}, Option Value: ${optionValue.name} - ` +
                  `Has been assigned with different SKU - "${chalk.bold(sku)}. Original Value - "${optionValue.sku}"`
                ))
              }

              existingValueSKUs.push(sku)

              var attributes: any = {}
              if (optionValue.priceOnApplication) {
                attributes.price_on_application = true
              }
              attributes.cost = optionValue.cost

              return {
                optionValue: magentoOptionValue,
                attributeOption: {
                  label: attributeOptionLabel
                },
                sku,
                attributes
              }
            }),
        })
      }

      if (configurableOptions.length === 0) {
        console.log(chalk.yellow(`Product ${product.sku} is skipped, because it has only customisable options`))
        continue
      }

      conversionConfig.push({
        sku: product.sku,
        price: product.magentoProduct.price,
        attributes: {
          cost: product.cost,
        },
        configurableOptions
      })
    }

    if (program.saveFolder) {
      const fs = require('fs')
      const { bigLog } = require('@m2dt/core/dist/util/bigLog')

      for (let conversionProduct of conversionConfig) {
        let filenameSafeSku = encodeURIComponent(conversionProduct.sku)

        const filename = `${program.saveFolder}/${filenameSafeSku}.json`
        fs.writeFile(filename, JSON.stringify(conversionProduct, null, 2), (err) => {
          if (err) {
            return console.log(err)
          }
        })
      }

      bigLog(`Saved ${conversionConfig.length} products to ${program.saveFolder}`)

      return
    }

    console.log(JSON.stringify(products, null, 2))
  })

interface SheetProduct {
  sku: string,
  name?: string,
  cost: number|null,
  rowStart: number,
  rowEnd: number,
  options: SheetProductOption[],
  magentoProduct?: any,
}

interface SheetProductOption {
  name: string,
  values: SheetProductOptionValue[],
  magentoOption?: MageProductOption,
  attributeCode?: string,
}

interface SheetProductOptionValue {
  name: string,
  sku: string|null,
  cost: number,
  priceOnApplication: boolean,
  isConfigurableOption: boolean,
  exists?: boolean,
  supplier?: string,
  magentoOptionValueId?: number,
}
