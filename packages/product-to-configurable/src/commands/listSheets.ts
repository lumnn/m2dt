import { M2DTCommand } from '@m2dt/core/dist/commander/M2DTCommand'
import XLSX from 'xlsx'

const program = new M2DTCommand()

export default program
  .arguments('<file>')
  .description("Lists spreadsheet sheet names")
  .action(async (file: string) => {
    var workbook = XLSX.readFile(file, {
      bookSheets: true
    })

    for (var name of workbook.SheetNames) {
      process.stdout.write(name + '\n')
    }
  })
