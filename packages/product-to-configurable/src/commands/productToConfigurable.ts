import { M2DTCommand } from '@m2dt/core/dist/commander/M2DTCommand'
import inquirer from 'inquirer'
import chalk from 'chalk'
import { createForConfigurableAction } from '@m2dt/core/dist/commands/attribute/createForConfigurable'
import { addOptionAction } from '@m2dt/core/dist/commands/attribute/addOption'
import type { ListChoiceOptions as InquirerChoice } from 'inquirer'
import type MagentoApi from 'magento2-api-wrapper'
import { ProductConversionConfig } from './fromConfig'
import { selectMagentoAttribute } from '../question/selectMagentoAttribute'

const program = new M2DTCommand()

export default program
  .arguments('<productSku>')
  .description('Creates a configuration which can be used to convert product into configurable one')
  .option('-s, --save-folder <path>')
  .action(async (productSku: string, program) => {
    const magento = await program.parent.getMagentoInstance()
    const product = await magento.get(`products/${productSku}`)

    if (product.type_id !== 'simple') {
      throw new Error(`Product type must be simple. Got "${product.type_id}"`)
    }

    if (product.options.length === 0) {
      throw new Error(`Product has no options`)
    }

    console.log(chalk.bgBlue("Select existing options to convert"))

    var optionsToConvert = await askWhichOptionsConvert(product)

    // add type?
    var saveData: ProductConversionConfig = {
      sku: product.sku,
      price: product.price,
      configurableOptions: [],
      options: [],
      attributes: {},
    }

    for (var i = 0; i < optionsToConvert.length; i++) {
      let option = optionsToConvert[i]

      console.log(chalk.bgBlue(`Configuring option: ${option.title}`))
      let attribute = await selectMagentoAttribute(magento, {
        allowNull: true,
        nullLabel: "-- Create New --",
        message: `Select attribute to replace "${option.title}" option`
      })

      if (!attribute) {
        try {
          attribute = await createForConfigurableAction(null, program)
        } catch (e) {
          console.log(chalk.bgRed("Failed to create new attribute."))
          throw e
        }

        if (!attribute) {
          throw new Error("New attribute was not created?")
        }
      }

      // add type?
      let valuesMapping: Array<any> = []

      for (var j = 0; j < option.values.length; j++) {
        let optionValue = option.values[j]

        let attributeValueAndOptions = await askAttributeValueAndConfig(attribute, {
          optionValue,
          message: `Choose attribute option for ${optionValue.title}`
        })

        if (attributeValueAndOptions.attributeOption === 'new') {
          attributeValueAndOptions.attributeOption = await addOptionAction(attribute.attribute_code, optionValue.title, program)
        }

        valuesMapping.push({
          optionValue: optionValue,
          ...attributeValueAndOptions
        })
      }

      saveData.configurableOptions.push({
        option,
        attribute,
        valuesMapping
      })
    }

    console.log(chalk.bgBlue("Adding new attributes"))

    do {
      var attribute = await selectMagentoAttribute(magento, { allowNull: true, message: 'Add New Attribute? Choose "No Value" to stop adding attributes' })

      if (!attribute) {
        break
      }

      // type?
      let valuesMapping: Array<any> = []

      do {
        var attributeValueAndConfig = await askAttributeValueAndConfig(attribute, {
          allowNull: true,
          message: `Add attribute Option. Choose "No Value" to stop adding values`
        })

        if (attributeValueAndConfig) {
          valuesMapping.push(attributeValueAndConfig)
        }
      } while(attributeValueAndConfig)

      if (valuesMapping.length === 0) {
        console.log("No values selected. Skipping...")
        break
      }

      saveData.configurableOptions.push({
        option: null,
        attribute: attribute,
        valuesMapping
      })
    } while (true)

    console.log(chalk.bgBlue("Choose order of options"))

    var configurableOptions = [...saveData.configurableOptions]
    var orderedOptions: Array<any> = []

    while (configurableOptions.length) {
      const { option } = await inquirer.prompt([{
        type: 'list', name: 'option', message: 'Choose next option', choices: configurableOptions.map(option => ({
          name: option.attribute.attribute_code,
          value: option
        }))
      }])

      configurableOptions.splice(configurableOptions.indexOf(option), 1)

      orderedOptions.push(option)
    }

    saveData.configurableOptions = orderedOptions

    if (program.saveFolder) {
      const fs = require('fs')
      const filename = `${program.saveFolder}/${product.sku}.json`
      const { bigLog } = require('@m2dt/core/dist/util/bigLog')

      fs.writeFile(filename, JSON.stringify(saveData, null, 2), (err) => {
        if (err) {
          return console.log(err)
        }

        bigLog(`Saved to ${filename}`)
      })

      return
    }

    console.log("You didn't choose to save data. Here it is:")
    console.log()
    console.log(JSON.stringify(saveData, null, 2))
  })

/**
 * Asks customer to select options from 
 */
async function askWhichOptionsConvert(product) {
  var answers = await inquirer.prompt([
    {
      type: 'checkbox',
      name: 'options',
      message: 'Which options should be converted to configurable ones?',
      choices: product.options.map(option => {
        var values = option.values.map(value => value.title).join(', ')

        return {
          name: `${option.title} - ` + (!option.is_require ? 'NOT REQUIRED ' : '') + chalk.gray(`#${option.option_id} (type: ${option.type}, values: ${values})`),
          value: option
        }
      })
    }
  ])

  return answers.options
}

var nullChoice: InquirerChoice = {
  name: '-- No value --',
  value: null
}

var newAttributeOptionChoice: InquirerChoice = {
  name: '-- Create New Value --',
  value: 'new'
}

interface AttributeValueAndConfigOptions {
  allowNull?: boolean
  optionValue?: any
  message?: string
}

/**
 * Function that asks user to select attribute option and then sku and price
 * for that attribute option
 */
async function askAttributeValueAndConfig(attribute, options: AttributeValueAndConfigOptions = { allowNull: false, message: `Choose attribute option` }) {
  var choices = attribute.options
    .filter(option => option.value)
    .map(option => ({
      name: option.label,
      value: option
    }))

  var defaultChoice: InquirerChoice|null = null

  if (options.optionValue) {
    choices.unshift(newAttributeOptionChoice)
    defaultChoice = choices.find(choice => choice.name.toLowerCase().trim() === options.optionValue.title.toLowerCase().trim())
  }

  if (options.allowNull) {
    choices.unshift(nullChoice)
  }

  var answers = await inquirer.prompt([
    {
      type: 'list',
      name: 'attributeOption',
      message: options.message,
      default: defaultChoice ? defaultChoice.value : null,
      choices
    },
    {
      type: 'input',
      name: 'sku',
      message: `Option SKU`,
      when: answers => answers.attributeOption,
    },
    {
      type: 'input',
      name: 'price',
      message: `Option Price`,
      default: 0,
      when: answers => answers.attributeOption,
      validate: value => {
        value = +value

        return Number.isFinite(value) && !Number.isNaN(value) ? true : 'Invalid number'
      }
    }
  ])

  return answers.attributeOption ? answers : null
}

export { program as productToConfigurable }
