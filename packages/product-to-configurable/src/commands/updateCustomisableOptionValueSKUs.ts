import { M2DTCommand } from '@m2dt/core/dist/commander/M2DTCommand'
import { read as readSpreadsheet } from '../spreadsheet/read'
import { chooseSheet } from '../spreadsheet/chooseSheet'
const program = new M2DTCommand()

export default program
  .arguments('<file> [sheetName]')
  .description('Updates customisable options in magento with the data from spreadsheet')
  .action(async (file: string, sheetName: undefined|string, program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()
    const products = readSpreadsheet(file, sheetName || await chooseSheet(file))

    const productSkus = products.map(product => product.sku)

    const magentoProducts: any = await magento.get('products', {
      params: {
        fields: 'items[sku,options]',
        searchCriteria: {
          filterGroups: [{
            filters: [{
              condition_type: 'in',
              field: 'sku',
              value: productSkus.join(',')
            }]
          }]
        }
      }
    })

    for (const product of products) {
      var mageProduct = magentoProducts.items ? magentoProducts.items.find(mageProduct => mageProduct.sku === product.sku) : null

      if (!mageProduct) {
        program.logger.warning(`Product with sku "${product.sku}" was not found in magento`)
        continue
      }

      for (const option of product.options) {
        var mageOption = mageProduct
          ? mageProduct.options.find(mageOption => mageOption.title.toLowerCase().trim() === option.name.toLowerCase().trim())
          : null

        if (!mageOption && mageProduct) {
          program.logger.warning(`Product Option "${option.name}" for product with sku "${product.sku}" was not found`)
        }

        var optionUpdate: boolean = false

        for (const value of option.values) {
          if (value.isConfigurableOption) {
            continue
          }

          if (!value.sku) {
            continue
          }

          var mageValue = mageOption
            ? mageOption.values.find(mageValue => mageValue.title.toLowerCase().trim() === value.name.toLowerCase().trim())
            : null

          if (!mageValue) {
            program.logger.error(`Product Option Value "${value.name}" for option "${option.name}" for product with sku "${product.sku}" was not found. Product (${value.sku}) won't be created`)
            continue
          }

          if (mageValue.sku === value.sku) {
            continue
          }

          program.logger.info(`${product.sku}: "${option.name}" - "${value.name}" will get updated SKU ${value.sku}`)

          mageValue.sku = value.sku
          optionUpdate = true
        }

        if (optionUpdate) {
          await magento.put(`products/options/${mageOption.option_id}`, { option: mageOption })
          program.logger.notice(`${product.sku}: "${option.name}" - Updated with new SKUs for values`)
        }
      }
    }
  })
