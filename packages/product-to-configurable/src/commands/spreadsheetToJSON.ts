import { M2DTCommand } from '@m2dt/core/dist/commander/M2DTCommand'
import { read as readSpreadsheet } from '../spreadsheet/read'
import { chooseSheet } from '../spreadsheet/chooseSheet'

const program = new M2DTCommand()

export default program
  .arguments('<file> [sheetName]')
  .description('Reads spreadsheet products and returns JSON')
  .action(async (file: string, sheetName: string) => {
    const products = readSpreadsheet(file, sheetName || await chooseSheet(file))
    console.log(JSON.stringify(products, null, 2))
  })
