import { M2DTCommand } from '@m2dt/core/dist/commander/M2DTCommand'
import { read as readSpreadsheet } from '../spreadsheet/read'
import { chooseSheet } from '../spreadsheet/chooseSheet'
import axios from 'axios'

const program = new M2DTCommand()

export default program
  .arguments('<file> [manufacturer]')
  .description("Creates products for all Customisable Options from spreadsheet")
  .action(async (file: string, manufacturer: undefined|string, program: M2DTCommand) => {
    const attributeSetId = 32

    const sheetName = manufacturer
      ? manufacturer.replace(/ /g, '_')
      : await chooseSheet(file)

    if (!manufacturer) {
      manufacturer = sheetName.replace(/_/g, ' ')
    }

    const products = readSpreadsheet(file, sheetName)
    const magento = await program.getMagentoInstance()

    const productSkus = products.map(product => product.sku)

    const magentoProducts: any = await magento.get('products', {
      params: {
        fields: 'items[sku,options]',
        searchCriteria: {
          filterGroups: [{
            filters: [{
              condition_type: 'in',
              field: 'sku',
              value: productSkus.join(',')
            }]
          }]
        }
      }
    })

    var newCustomisableOptionProducts: NewCustomisableOptionProduct[] = []

    for (const product of products) {
      var mageProduct = magentoProducts.items ? magentoProducts.items.find(mageProduct => mageProduct.sku === product.sku) : null

      if (!mageProduct) {
        program.logger.warning(`Product with sku "${product.sku}" was not found in magento`)
      }

      for (const option of product.options) {
        var mageOption = mageProduct
          ? mageProduct.options.find(mageOption => mageOption.title.toLowerCase().trim() === option.name.toLowerCase().trim())
          : null

        if (!mageOption && mageProduct) {
          program.logger.warning(`Product Option "${option.name}" for product with sku "${product.sku}" was not found`)
        }

        for (const value of option.values) {
          if (value.isConfigurableOption) {
            continue
          }

          if (!value.sku) {
            continue
          }

          let newSkuCustomisableOptionProduct = newCustomisableOptionProducts.find(newOptionProduct => newOptionProduct.sku === value.sku)

          if (newSkuCustomisableOptionProduct) {
            if (Math.abs(newSkuCustomisableOptionProduct.cost - value.cost) > 0.0001) {
              program.logger.error(`Option with "${value.sku}" sku has multiple different prices`)
            }

            if (newSkuCustomisableOptionProduct.supplier !== value.supplier) {
              program.logger.error(`Option with "${value.sku}" sku has multiple different suppliers`)
            }

            if (newSkuCustomisableOptionProduct.name !== value.name) {
              program.logger.info(`Option with "${value.sku}" sku has multiple different names`)
            }

            continue
          }

          var mageValue = mageOption
            ? mageOption.values.find(mageValue => mageValue.title.toLowerCase().trim() === value.name.toLowerCase().trim())
            : null

          if (!mageValue) {
            program.logger.error(`Product Option Value "${value.name}" for option "${option.name}" for product with sku "${product.sku}" was not found. Product (${value.sku}) won't be created`)
            continue
          }

          var mageProduct: any = null

          try {
            mageProduct = await magento.get(`products/${encodeURIComponent(value.sku)}`)
          } catch (e) {
            if (!axios.isAxiosError(e) || !e.response || e.response.status !== 404) {
              throw e
            }
          }

          if (mageProduct && mageProduct.attribute_set_id !== attributeSetId) {
            program.logger.info(`Product with sku ${value.sku} exist. Skipping`)

            continue
          } else if (mageProduct) {
            program.logger.debug(`Product with sku ${value.sku} exist, but it will be updated as it has same attribute group`)
          }

          let regexSku = value.sku.replace(/[.*+?^${}()|[\]\\-]/g, '\\$&')

          let name = value.name

          name = name
            .replace(new RegExp('\\s*\\(?' + regexSku + '\\)?\\s*', 'i'), '')
            .replace(/^\s*(Add|Include|Optional)\s+/, '')

          if (["add", "include"].includes(name.toLowerCase().trim()) && option.values.length === 1) {
            name = option.name.replace(/^\s*(Add|Optional)\s+/, '')
            program.logger.debug(`Using option group name as product name for "${value.sku}", "${name}"`)
          }

          console.log(name)

          newCustomisableOptionProducts.push({
            name,
            sku: value.sku,
            supplier: value.supplier,
            cost: value.cost,
          })
        }
      }
    }

    const manufacturerResponse: any = await magento.get(`products/attributes/manufacturer`)
    const manufacturerIds: any = {}

    for (const option of manufacturerResponse.options) {
      manufacturerIds[option.label] = option.value
    }

    for (const product of newCustomisableOptionProducts) {
      if (!manufacturerIds[product.supplier || manufacturer]) {
        throw new Error(`Missing manufacturer with name ${product.supplier}`)
      }
    }

    for (const product of newCustomisableOptionProducts) {
      await magento.post('products', {
        product: {
          sku: product.sku,
          name: product.name,
          attribute_set_id: attributeSetId,
          price: product.cost + (product.cost * 0.5),
          status: 1,
          visibility: 0,
          type_id: 'simple',
          extension_attributes: {
            stock_item: {
              backorders: 1,
              manage_stock: true,
            }
          },
          custom_attributes: [
            { attribute_code: 'cost', value: product.cost },
            { attribute_code: 'manufacturer', value: manufacturerIds[product.supplier || manufacturer] }
          ]
        }
      }, {
        storeCode: 'all'
      })
    }

    // output a list of newly created products
    newCustomisableOptionProducts
      .map(product => product.sku)
      .forEach(v => process.stdout.write(v + '\n'))
  })

interface NewCustomisableOptionProduct {
  sku: string,
  name: string,
  supplier?: string,
  cost: number
}
