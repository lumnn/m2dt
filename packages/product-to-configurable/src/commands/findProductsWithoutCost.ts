import { M2DTCommand } from '@m2dt/core/dist/commander/M2DTCommand'

const program = new M2DTCommand()

export default program
  .description("Find magento products and options without SKU assigned")
  .action(async (program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()
    const limit = 500
    let currentPage = 1
    var totalCount: Number = 0
    var processed = 0

    const result: any[] = []

    do {
      const productsResponse: any = await magento.get('products', {
        params: {
          fields: 'items[sku,name,price,custom_attributes,status],total_count',
          searchCriteria: {
            currentPage: currentPage++,
            pageSize: limit,
            filterGroups: [{
              filters: [{
                condition_type: 'eq',
                field: 'status',
                value: 1
              }]
            }]
          }
        }
      })

      if (totalCount === 0) {
        totalCount = productsResponse.total_count
      }

      const products = productsResponse.items

      for (const product of products) {
        const poaAttr = product.custom_attributes.find(attr => attr.attribute_code === 'price_on_application')
        const costAttr = product.custom_attributes.find(attr => attr.attribute_code === 'cost')

        if (costAttr && costAttr.value != 0) {
          continue
        }

        const manufacturer = product.custom_attributes.find(attr => attr.attribute_code === 'manufacturer')
        const manufacturerId = manufacturer ? manufacturer.value : null

        result.push({
          sku: product.sku,
          name: product.name,
          price: product.price,
          manufacturerId: manufacturerId,
          cost: costAttr ? costAttr.value : null,
          poa: poaAttr ? poaAttr.value : null
        })
      }

      processed += products.length
    } while (processed < totalCount)

    process.stdout.write(JSON.stringify(result, null, 2))
  })
