import { M2DTCommand } from '@m2dt/core/dist/commander/M2DTCommand'

const program = new M2DTCommand()

export default program
  .description("Find magento products and options without SKU assigned")
  .action(async (program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()
    const limit = 500
    let currentPage = 1
    var totalCount: Number = 0
    var processed = 0

    const result: any[] = []

    do {
      const productsResponse: any = await magento.get('products', {
        params: {
          fields: 'items[sku,options,custom_attributes,status],total_count',
          searchCriteria: {
            currentPage: currentPage++,
            pageSize: limit,
            filterGroups: [{
              filters: [{
                condition_type: 'eq',
                field: 'status',
                value: 1
              }]
            }]
          }
        }
      })

      if (totalCount === 0) {
        totalCount = productsResponse.total_count
      }

      const products = productsResponse.items

      for (const product of products) {
        const poaAttr = product.custom_attributes.find(attr => attr.attribute_code === 'price_on_application')

        if (poaAttr && poaAttr.value === "1") {
          continue
        }

        const manufacturer = product.custom_attributes.find(attr => attr.attribute_code === 'manufacturer')
        const manufacturerId = manufacturer ? manufacturer.value : null

        for (const option of product.options) {
          if (!option.values) {
            continue
          }

          for (const optionValue of option.values) {
            const guessedSku = optionValue.title.match(/(?:^\(([a-z0-9\.\-\_\\\/]+)\))|(?:\(([a-z0-9\.\-\_\\\/]+)\)$)/i)

            if (!optionValue.sku) {
              result.push({
                optionId: optionValue.option_type_id,
                productSku: product.sku,
                option: option.title,
                valueTitle: optionValue.title,
                price: optionValue.price,
                manufacturer: manufacturerId,
                status: product.status,
                guessedSku: guessedSku ? (guessedSku[1] || guessedSku[2]) : null
              })
            }
          }
        }
      }

      processed += products.length
    } while (processed < totalCount)

    const guessedSkus = new Set(result.filter(r => !!r.guessedSku).map(r => r.guessedSku))

    const guessedProducts: any = await magento.get('products', {
      params: {
        fields: 'items[sku,name,options,custom_attributes,status],total_count',
        searchCriteria: {
          currentPage: currentPage++,
          pageSize: limit,
          filterGroups: [{
            filters: [{
              condition_type: 'in',
              field: 'sku',
              value: Array.from(guessedSkus).join(',')
            }]
          }]
        }
      }
    })

    for (const option of result) {
      option.guessedProduct = null

      if (!option.guessedSku || !guessedProducts.items) {
        continue
      }

      const guessedProduct = guessedProducts.items.find(p => p.sku === option.guessedSku)
      option.guessedProduct = guessedProduct ? guessedProduct.name : null
    }

    process.stdout.write(JSON.stringify(result, null, 2))
  })
