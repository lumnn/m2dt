import M2DTCommand from '../commander/M2DTCommand'
import isCommandsPlugin from '../plugin/isCommandsPlugin'
import { load as loadConfig } from '../config/loader'

require('dotenv').config()

const program = new M2DTCommand();

program
  .option('-c, --config <name>', 'Config file')
  .option('-P, --profile <name>', 'Magento profile to use')
  .option('-S, --store <storeCode>', 'Magento store code to connect to', 'all')
  .option('-v, --verbose', 'Verbose output')
  .addCommand(require('../commands/apiGet'))
  .addCommand(require('../commands/api').name('api'))
  .addCommand(require('../commands/compare').default.name('compare'))
  .addCommand(require('../commands/get'))
  .addCommand(require('../commands/list'))
  .addCommand(require('../commands/put'))
  .addCommand(require('../commands/post'))
  .addCommand(require('../commands/test').name('test'))
  .addCommand(require('../commands/attribute/assignToSet').default.name('attribute:assign-to-set'))
  .addCommand(require('../commands/attribute/createForConfigurable').default.name('attribute:create-for-configurable'))
  .addCommand(require('../commands/attribute/addOption').default.name('attribute:add-option'))
  .addCommand(require('../commands/attribute/removeOption').default.name('attribute:remove-option'))
  .addCommand(require('../commands/category/addProducts').default.name('category:add-products'))
  .addCommand(require('../commands/category/removeProducts').default.name('category:remove-products'))
  .addCommand(require('../commands/product/assignToWebsite').default.name('product:assign-to-website'))
  .addCommand(require('../commands/product/deleteConfigurableAttribute').default.name('product:delete-configurable-attribute'))
  .addCommand(require('../commands/product/findWithAttribute').default.name('product:find-with-attribute'))
  .addCommand(require('../commands/product/findWithEmptyAttribute').default.name('product:find-with-empty-attribute'))
  .addCommand(require('../commands/product/getScopeDiff').default.name('product:get-scope-diff'))
  .addCommand(require('../commands/product/renameOptionValue').default.name('product:rename-option-value'))
  .addCommand(require('../commands/product/setAttribute').default.name('product:set-attribute'))
  .addCommand(require('../commands/product/setAttributeIfEmpty').default.name('product:set-attribute-if-empty'))
  .addCommand(require('../commands/product/updateOption').default.name('product:update-option'))
  .addCommand(require('../commands/product/updateOptionValue').default.name('product:update-option-value'))
  .addCommand(require('../commands/product/updateOptionValueSortOrder').default.name('product:update-option-value-sort-order'))
  .addCommand(require('../commands/product/updateScopeAttributes').default.name('product:update-scope-attributes'))
  .addCommand(require('../commands/shipping/estimate').default.name('shipping:estimate'))

program.on('option:verbose', function (this: M2DTCommand) {
  process.env.VERBOSE = this.verbose;
})

program._afterOptions = function () {
  this.configArg = config
  var { config, path } = loadConfig(this.config)
  this.configObject = config
  this.configPath = path

  var registeredNamespaces: string[] = []

  this.config.plugins = this.config.plugins || []

  this.config.plugins.forEach(plugin => {
    if (isCommandsPlugin(plugin)) {
      const namespace: string = plugin.getNamespace()

      if (registeredNamespaces.includes(namespace)) {
        throw new Error(`There is already registered plugin with namespace ${namespace}`)
      }

      plugin.getCommands().forEach(command => {
        const name = [namespace]
        const commandName = command.name()
        if (commandName) {
          name.push(commandName)
        }

        this.addCommand(command.name(name.join(':')))
      })
    }
  })

  if (process.env.VERBOSE) {
    console.log("Config:  ", this.configPath)
    console.log("Profile: ", this.profile)
    console.log("Store:   ", this.store)
  }
}

program.parseAsync(process.argv)
  .catch(function (e) {
    if (process.env.VERBOSE) {
      console.error(e)
    }

    if (e.exitMessage !== false) {
      console.log("Program has stopped due to an error.")
    }

    console.log(e.message)

    const { handleCliApiError } = require('../util/handleCliApiError')

    handleCliApiError(e)

    process.exit(e.exitCode || 1)
  })
