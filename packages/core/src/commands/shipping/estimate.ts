import { M2DTCommand } from "../../commander/M2DTCommand"

const program = new M2DTCommand()

program
  .description("Check shipping for a product")
  .arguments('<productSku>')
  .option('-p, --postcode <postcode>', 'Delivery Postcode. For example: DN22 7WF')
  .option('-C, --country <country>', 'Delivery Country. For example: GB')
  .option('-r, --region <region>', 'Delivery Region. For example: Nottinghamshire')
  .action(async (productSku: string, program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()
    const safeProductSku = encodeURIComponent(productSku)

    const product = await magento.get(`products/${safeProductSku}`)

    var cartId = await magento.post('/guest-carts')

    const customOptions: any = []
    for (var option of product.options) {
      if (!option.is_require) {
        continue
      }

      customOptions.push({
        option_id: option.option_id,
        option_value: option.values[0].option_type_id
      })
    }

    const configurableItemOptions: any = []
    for (var option of product.extension_attributes.configurable_product_options) {
      configurableItemOptions.push({
        option_id: option.attribute_id,
        option_value: option.values[0].value_index
      })
    }

    await magento.post(`/guest-carts/${cartId}/items`, {
      cartItem: {
        sku: product.sku,
        qty: 1,
        quote_id: cartId,
        product_option: {
          extension_attributes: {
            custom_options: customOptions,
            configurable_item_options: configurableItemOptions
          }
        }
      }
    })

    const address: any = {}

    if (program.region) {
      address.region = program.region
    }

    if (program.country) {
      address.country_id = program.country
    }

    if (program.postcode) {
      address.postcode = program.postcode
    }

    const result = await magento.post(`/guest-carts/${cartId}/estimate-shipping-methods`, {
      address
    })

    console.log(result)
  })

export default program
