import { readStdinOnce } from "../util/readStdinOnce"

const { Command } = require('commander')
const path = require('path')
const getMagentoInstance = require('../util/getMagentoInstance').default

const program = new Command()

program
  .arguments('<method> <path>')
  .description("Allows to quickly execute any HTTP request on API")
  .option('-B, --reset-base-url', 'Resets base url to exclude /rest/V1 part')
  .action(async (method: string, path, program) => {
    const magento = await program.parent.getMagentoInstance()

    var params: any = {}

    if (program.resetBaseUrl) {
      params.baseURL = magento.apiParams.url
    }

    var data: any = null

    if (["post", "put"].includes(method.toLowerCase())) {
      let input = await readStdinOnce()
      data = JSON.parse(input)

      if (process.env.VERBOSE) {
        console.log("Data:")
        console.log(data)
      }
    }

    var result = await magento.request({
      method,
      url: path,
      params,
      storeCode: program.parent.store,
      data
    })

    console.log(JSON.stringify(result, null, 2))

    return
  })

module.exports = program
