import { M2DTCommand } from '../commander/M2DTCommand'
const setupFieldsOption = require('./options/fields')
import path from 'path'
const scriptName = path.basename(__filename).replace('.js', '')

const program = new M2DTCommand()

setupFieldsOption(program)

program
  .name(scriptName)
  .description("Get resource by it's id. May be SKU for products")
  .arguments('<resource> <id>')
  .option('-p, --pretty', 'Try to improve output for better readability. Result will not be 100% JSON')
  .action(async function(this: M2DTCommand, resource, id, program) {
    const chalk = require('chalk')
    const normalizeResourceName = require('../magento/normalizeResourceName').default

    const magento = await program.parent.getMagentoInstance()

    resource = normalizeResourceName(resource)
    if (process.env.VERBOSE) {
      console.log(chalk.blue(`Using resource "${resource}"`))
    }

    const resourceConfig = await this.loadResourceConfig(resource)

    var getUrl = resourceConfig && resourceConfig.endpoints && resourceConfig.endpoints.get
      ? resourceConfig.endpoints.get
      : resource

    var result = await magento.get(`${getUrl}/${encodeURIComponent(id)}`, {
      storeCode: program.parent.store,
      params: { fields: program.fields },
    })

    // after plugin?

    if (program.pretty) {
      if (resourceConfig.prettify) {
        result = resourceConfig.prettify(result)
      }

      console.dir(result, { depth: null })

      return
    }

    console.log(JSON.stringify(result, null, 2))
  })

module.exports = program
