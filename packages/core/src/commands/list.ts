import { M2DTCommand } from '../commander/M2DTCommand'
import path from 'path'
import setupFieldsOption from './options/fields'
const scriptName = path.basename(__filename).replace('.js', '')

const program = new M2DTCommand()

setupFieldsOption(program)

program
  .name(scriptName)
  .arguments('<resource>')
  .option('-p, --page <number>', 'Page to show', '1')
  .option('-l, --limit <number>', 'Limit number of rows', '50')
  .option('-C, --columns <columns...>', 'Columns to show')
  .option('-f, --filters <filterGroups...>', 'Filtering results')
  .option('-r, --raw', 'Output in plain JSON')
  .action(async function (this: M2DTCommand, resource, program) {
    const chalk = require('chalk')
    const normalizeResourceName = require('../magento/normalizeResourceName').default
    const parseFilterGroupsStrings = require('../util/parseFilterGroupsStrings')

    const magento = await program.parent.getMagentoInstance()

    // this may go to the event?
    resource = normalizeResourceName(resource)
    if (process.env.VERBOSE) {
      console.log(chalk.blue(`Using resource "${resource}"`))
    }

    const resourceConfig = await this.loadResourceConfig(resource)

    // before plugin?

    var listUrl = resourceConfig && resourceConfig.endpoints && resourceConfig.endpoints.list
      ? resourceConfig.endpoints.list
      : resource

    var filterGroups = program.filters ? parseFilterGroupsStrings(program.filters) : []

    if (process.env.VERBOSE) {
      console.log(chalk.gray(`GET ${listUrl}`))

      filterGroups.forEach((group, index) => {
        var filters = group.filters
          .map(f => `${f.field} ${f.condition_type} ${f.value || ''}`.trim())
          .join(' OR ')
        console.log(chalk.yellow(`Filter Group ${index + 1}: ${filters}`))
      })
    }

    var result = await magento.get(listUrl, {
      storeCode: program.parent.store,
      params: {
        fields: program.fields,
        searchCriteria: {
          pageSize: program.limit,
          currentPage: program.page,
          filterGroups
        }
      }
    })

    // after plugin?

    if (program.raw) {
      console.log(JSON.stringify(result, null, 2))
      return
    }

    console.log(`Total Results: ${chalk.bold(result.total_count)}`)
    console.log(`Page:          ${chalk.bold(program.page)} of ${Math.ceil(result.total_count / program.limit)}`)
    console.log(`Limit:         ${chalk.bold(program.limit)}`)

    if (!program.columns) {
      if (resourceConfig && resourceConfig.columns) {
        program.columns = resourceConfig.columns
      } else {
        var allColumns = Object.keys(result.items[0])
        program.columns = allColumns.slice(0, 5)
      }
    }

    var values = result.items

    if (program.page > 1 && result.total_count > program.limit) {
      // this is really a trick to start indexing so console.table is more informative about index of the result
      values = []

      var maxPage = Math.ceil(result.total_count / program.limit)
      var zeroIndexPage = Math.min((maxPage - 1), ((program.page - 1)))
      var index = zeroIndexPage * program.limit - 1

      values[index] = null
      values.push(...result.items)
      delete values[index]
    }

    console.table(values, program.columns)
  })

module.exports = program
