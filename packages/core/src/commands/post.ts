import { M2DTCommand } from '../commander/M2DTCommand'
import path from 'path'
const scriptName = path.basename(__filename).replace('.js', '')

const program = new M2DTCommand()

program
  .name(scriptName)
  .description("Update resource")
  .arguments('<resource>')
  .action(async function(this: M2DTCommand, resource, program) {
    const { readStdinOnce } = require('../util/readStdinOnce')
    const magento = await this.getMagentoInstance()
    const resourceConfig = await this.loadResourceConfig(resource)

    var input = await readStdinOnce()
    input = JSON.parse(input)

    var basePath = resourceConfig.endpoints && resourceConfig.endpoints.base
      ? resourceConfig.endpoints.base
      : resource

    var singularName = resourceConfig.singularName || resource

    var saveData = {}
    saveData[singularName] = input

    var result = await magento.post(
      `${basePath}`,
      saveData,
      { storeCode: program.parent.store }
    )

    // after plugin?

    if (program.raw) {
      console.log(JSON.stringify(result, null, 2))

      return
    }

    console.log(result)
  })

module.exports = program
