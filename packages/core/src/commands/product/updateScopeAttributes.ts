import { M2DTCommand } from "../../commander/M2DTCommand"
import { attributesToObject } from "../../magento/product/attributesToObject"
import { ProductAttribute } from "../../magento/types/ProductAttribute"

const program = new M2DTCommand()

program
  .description("")
  // .option('--scope <scope>', 'Scope - website or store')
  .option('--scope-code <scopeCode>', 'Scope Code')
  .action(async (program: M2DTCommand) => {
    // const scope = program.scope
    const scopeCode = program.scopeCode
    const magento = await program.getMagentoInstance()
    const { readStdinOnce } = require('../../util/readStdinOnce')

    var input = await readStdinOnce()
    input = JSON.parse(input)

    if (!input.sku) {
      throw new Error("Missing SKU field in input")
    }

    const attributeCodes = input.custom_attributes.map(attribute => attribute.attribute_code)
    const attributes = {}

    for (const attributeCode of attributeCodes) {
      attributes[attributeCode] = await magento.get(`products/attributes/${attributeCode}`)
    }

    var scopeInputAttributes = input.custom_attributes.filter(productAttr => {
      const code = productAttr.attribute_code
      const attrConfig = attributes[code]

      return attrConfig.scope !== 'global'
    })

    const safeSku = encodeURIComponent(input.sku)
    const globalProduct = await magento.get(`products/${safeSku}`, { storeCode: 'all', params: { fields: 'sku,custom_attributes' } })
    const globalAttrs = attributesToObject(globalProduct.custom_attributes)

    const updateProduct = {
      sku: input.sku,
      custom_attributes: scopeInputAttributes.map((attr: ProductAttribute) => {
        const scopeInputAttrCode = attr.attribute_code

        // if the value we want to import is same as global, then use `null`
        // to make use of the global value
        if (globalAttrs[scopeInputAttrCode] !== undefined && globalAttrs[scopeInputAttrCode] === attr.value) {
          attr.value = null
        }

        return attr
      })
    }

    const updatedProduct = await magento.put(`products/${safeSku}`, { product: updateProduct }, {
      storeCode: scopeCode
    })

    console.log({
      sku: updatedProduct.sku,
      // filter only updated values
      custom_attributes: updatedProduct.custom_attributes.filter((updatedAttr: ProductAttribute) => {
        const attrCode = updatedAttr.attribute_code

        return updateProduct.custom_attributes.find(
          (updateAttribute: ProductAttribute) => updateAttribute.attribute_code === attrCode
        )
      })
    })
  })

export default program
