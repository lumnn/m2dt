import { M2DTCommand } from "../../commander/M2DTCommand"

const program = new M2DTCommand()

program
  .description("")
  .arguments('<productSku> <optionGroupTitle>')
  .option('-t, --title <title>', 'New Title')
  .option('-s, --sort <sort>', 'New Sort Order')
  .action(async (productSku: string, optionGroupTitle: string, program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()

    const product = await magento.get(`products/${encodeURIComponent(productSku)}`, {
      params: {
        fields: 'sku,options'
      }
    })

    const option = product.options.find(option => option.title.trim() === optionGroupTitle.trim())

    if (!option) {
      throw new Error(`Couldn't find option with title "${optionGroupTitle}"`)
    }

    if (program.title) {
      option.title = program.title
    }

    if (program.sort) {
      option.sort_order = program.sort
    }

    await magento.put(`products/options/${option.option_id}`, { option })
  })

export default program
