import handleCliApiError from "../../util/handleCliApiError"
import { M2DTCommand } from "../../commander/M2DTCommand"

const program = new M2DTCommand()

program
  .description("Find products whith specified attribute beeing missing or empty")
  .arguments('<attributeCode> <conditionType> <attributeValue>')
  .action(async (attributeCode: string, conditionType: string, attributeValue: string, program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()
    const limit = 500
    let currentPage = 1
    var totalCount: Number = 0
    var processed = 0

    do {
      const productsResponse: any = await magento.get('products', {
        params: {
          fields: 'items[sku,name,custom_attributes],total_count',
          searchCriteria: {
            currentPage: currentPage++,
            pageSize: limit,
            filterGroups: [{
              filters: [
                {
                  condition_type: conditionType,
                  field: attributeCode,
                  value: attributeValue
                }
              ]
            }]
          }
        }
      })

      if (totalCount === 0) {
        totalCount = productsResponse.total_count
      }

      const products = productsResponse.items

      for (const product of products) {
        const attribute = product.custom_attributes.find(attr => attr.attribute_code === attributeCode)

        console.log(`${product.sku}\t${attribute.value}`)
      }

      processed += products.length
    } while (processed < totalCount)
  })

export default program
