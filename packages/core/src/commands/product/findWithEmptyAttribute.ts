import handleCliApiError from "../../util/handleCliApiError"
import { M2DTCommand } from "../../commander/M2DTCommand"

const program = new M2DTCommand()

program
  .description("Find products whith specified attribute beeing missing or empty")
  .arguments('<attributeCode>')
  .action(async (attributeCode: string, program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()
    const limit = 500
    let currentPage = 1
    var totalCount: Number = 0
    var processed = 0

    do {
      const productsResponse: any = await magento.get('products', {
        params: {
          fields: 'items[sku,name,custom_attributes],total_count',
          searchCriteria: {
            currentPage: currentPage++,
            pageSize: limit,
            filterGroups: [{
              filters: [
                {
                  condition_type: 'null',
                  field: attributeCode,
                }
              ]
            }]
          }
        }
      })

      if (totalCount === 0) {
        totalCount = productsResponse.total_count
      }

      const products = productsResponse.items

      for (const product of products) {
        const attribute = product.custom_attributes.find(attr => attr.attribute_code === attributeCode)

        if (attribute && attribute.value.length > 0) {
          continue
        }

        console.log(product.sku)
      }

      processed += products.length
    } while (processed < totalCount)
  })

export default program
