import handleCliApiError from "../../util/handleCliApiError"
import { M2DTCommand } from "../../commander/M2DTCommand"
import Magento2Api from "magento2-api-wrapper"

const program = new M2DTCommand()

program
  .description("Assign product to website")
  .arguments('<sku> <website_id>')
  .action(async (sku: string, website_id: string, program: M2DTCommand) => {
    const magento: Magento2Api = await program.getMagentoInstance()

    const childs = await magento.get(`configurable-products/${encodeURIComponent(sku)}/children`)

    for (const child of childs) {
      await magento.post(`products/${encodeURIComponent(child.sku)}/websites`, {
        productWebsiteLink: {
          sku: child.sku,
          website_id
        }
      })

      process.stdout.write(`${child.sku} (child of ${sku}) is now assigned to ${website_id}\n`)
    }

    const response = await magento.post(`products/${encodeURIComponent(sku)}/websites`, {
      productWebsiteLink: {
        sku,
        website_id
      }
    })

    process.stdout.write(`${sku} is now assigned to ${website_id}\n`)
  })

export default program
