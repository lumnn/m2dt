import { M2DTCommand } from "../../commander/M2DTCommand"

const program = new M2DTCommand()

program
  .description("Updates product attribute")
  .arguments('<productSku> <attributeCode> <newValue>')
  .option('-f, --force', 'Force overwite value if the attribute already has a value')
  .option('-c, --with-children', 'Update the attribute also on children products if it\'s configurable')
  .action(async (productSku: string, attributeCode: string, newValue: string, program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()
    const safeProductSku = encodeURIComponent(productSku)

    const product = await magento.get(`products/${safeProductSku}`, {
      params: {
        fields: 'sku,custom_attributes,type_id'
      }
    })

    const safeAttributeCode = encodeURIComponent(attributeCode)
    const attribute = await magento.get(`products/attributes/${safeAttributeCode}`)

    if (attribute.options) {
      let optionValue = attribute.options.find(option => option.label === newValue)

      if (!optionValue) {
        throw new Error(`There is no option for attribute ${attributeCode} with label ${newValue}`)
      }

      newValue = optionValue.value
    }

    const updateProducts = [product]

    if (product.type_id === 'configurable' && program.withChildren) {
      const children = await magento.get(`configurable-products/${safeProductSku}/children`)
      updateProducts.push(...children)
    }

    for (const product of updateProducts) {
      const attribute = product.custom_attributes.find(attr => attr.attribute_code === attributeCode)

      if (!attribute || attribute.value === newValue) {
        continue
      }

      if (program.force !== true) {
        throw new Error(`Product "${productSku}" has already a value "${attribute.value}" in attribute`)
      }
    }

    var hadError = false

    for (const product of updateProducts) {
      const attribute = product.custom_attributes.find(attr => attr.attribute_code === attributeCode)

      if (attribute && attribute.value === newValue) {
        console.log(`${product.sku} - Already correct` + (product.sku !== productSku ? ' (child)' : ''))

        continue
      }

      const safeSku = encodeURIComponent(product.sku)

      const response = await magento.put(`products/${safeSku}`, {
        product: {
          custom_attributes: [
            {
              attribute_code: attributeCode,
              value: newValue
            }
          ]
        }
      })

      const updatedAttr = response.custom_attributes.find(attr => attr.attribute_code === attributeCode)

      if (!updatedAttr || updatedAttr.value !== newValue) {
        hadError = true
        process.stderr.write(`${product.sku} - For some reason the attribute did not update. It may not be in attribute set used by product\n`)
        continue
      }

      console.log(`${product.sku} - OK` + (product.sku !== productSku ? ' (child)' : ''))
    }

    if (hadError) {
      throw new Error("Not all products seem to be updated. Please check update manually.")
    }
  })

export default program
