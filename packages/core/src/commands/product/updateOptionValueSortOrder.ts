import { M2DTCommand } from "../../commander/M2DTCommand"

const program = new M2DTCommand()

program
  .arguments('<productSku> <optionTitle> <optionValueTitle> <newSortOrder>')
  .action(async (productSku: string, optionTitle: string, optionValueTitle: string, newSortOrder: number, program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()

    const product = await magento.get(`products/${encodeURIComponent(productSku)}`, {
      params: {
        fields: 'sku,options'
      }
    })

    const option = product.options.find(option => option.title === optionTitle)

    if (!option) {
      throw new Error(`Couldn't find option with title "${optionTitle}"`)
    }

    if (!option.values) {
      throw new Error(`API returned no option values`)
    }

    option.values.sort((a, b) => {
      var diff = a.sort_order - b.sort_order
      return diff !== 0 ? diff : a.title.localeCompare(b.title)
    })

    const optionValueIndex = option.values.findIndex(optionValue => optionValue.title === optionValueTitle)

    if (optionValueIndex === -1) {
      throw new Error(`Couldn't find option value with title "${optionValueTitle}"`)
    }

    const optionValue = option.values[optionValueIndex]

    optionValue.sort_order = newSortOrder

    await magento.put(`products/options/${option.option_id}`, { option })
  })

export default program
