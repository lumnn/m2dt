import { M2DTCommand } from "../../commander/M2DTCommand"
import { attributesToObject } from "../../magento/product/attributesToObject"

const program = new M2DTCommand()

program
  .description("Shows product changes applied to scope. Works only with scalar props and attributes")
  .arguments('<sku>')
  .action(async (sku: string, program: M2DTCommand) => {
    const storeCode = program.root.store

    if (!storeCode) {
      throw new Error("Missing value for --store option")
    }

    const magento = await program.getMagentoInstance()

    const safeSku = encodeURIComponent(sku)
    const globalProduct = await magento.get(`products/${safeSku}`, { storeCode: 'all', params: { fields: 'sku,custom_attributes' } })
    const scopeProduct = await magento.get(`products/${safeSku}`, { storeCode, params: { fields: 'sku,custom_attributes' } })
    const globalAttrs = attributesToObject(globalProduct.custom_attributes)
    const scopeAttrs = attributesToObject(scopeProduct.custom_attributes)

    const diff: any = {
      sku
    }

    for (const prop in scopeProduct) {
      if (typeof scopeProduct[prop] !== 'object' && scopeProduct[prop] !== globalProduct[prop]) {
        diff[prop] === scopeProduct[prop]
      }
    }

    for (const attr in scopeAttrs) {
      if (scopeAttrs[attr] === globalAttrs[attr]) {
        delete scopeAttrs[attr]
      }
    }

    diff.attributes = scopeAttrs

    process.stdout.write(JSON.stringify(diff, null, 2))
    process.stdout.write("\n")
  })

export default program
