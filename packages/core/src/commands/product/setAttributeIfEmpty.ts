import handleCliApiError from "../../util/handleCliApiError"
import { M2DTCommand } from "../../commander/M2DTCommand"
import axios from "axios";

const program = new M2DTCommand()

program
  .description("Updates all products attribute if it's empty")
  .arguments('<attributeCode> <newValue>')
  .action(async (attributeCode: string, newValue: string, program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()
    const limit = 500
    let currentPage = 1
    var totalCount: Number = 0
    var processed = 0

    do {
      const productsResponse: any = await magento.get('products', {
        params: {
          fields: 'items[sku,name,custom_attributes],total_count',
          searchCriteria: {
            currentPage: currentPage++,
            pageSize: limit,
            filterGroups: [{
              filters: [
                {
                  condition_type: 'eq',
                  field: 'status',
                  value: 1
                },
                {
                  condition_type: 'null',
                  field: attributeCode,
                }
              ]
            }]
          }
        }
      })

      if (totalCount === 0) {
        totalCount = productsResponse.total_count
      }

      const products = productsResponse.items

      for (const product of products) {
        const attribute = product.custom_attributes.find(attr => attr.attribute_code === attributeCode)

        if (attribute !== undefined) {
          console.log(`${product.sku} - Has a value of "${attribute.value}"`)
          continue
        }

        console.log(product.sku)

        try {
          await magento.put(`products/${encodeURIComponent(product.sku)}`, {
            product: {
              custom_attributes: [
                {
                  attribute_code: attributeCode,
                  value: newValue
                }
              ]
            }
          })
        } catch (e) {
          if (!axios.isAxiosError(e)) {
            throw e
          }

          try {
            handleCliApiError(e)
          } catch (e2) {
            console.log(e2)
          }
        }
      }

      processed += products.length
    } while (processed < totalCount)
  })

export default program
