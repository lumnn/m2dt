import { M2DTCommand } from "../../commander/M2DTCommand"

const program = new M2DTCommand()

program
  .description("")
  .arguments('<productSku> <optionTitle> <optionValueTitle> <newOptionValueTitle>')
  .action(async (productSku: string, optionTitle: string, optionValueTitle: string, newOptionValueTitle: string, program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()

    const product = await magento.get(`products/${encodeURIComponent(productSku)}`, {
      params: {
        fields: 'sku,options'
      }
    })

    const option = product.options.find(option => option.title.trim() === optionTitle.trim())

    if (!option) {
      throw new Error(`Couldn't find option with title "${optionTitle}"`)
    }

    if (!option.values) {
      throw new Error(`API returned no option values`)
    }

    const optionValue = option.values.find(optionValue => optionValue.title.trim() === optionValueTitle.trim())

    if (!optionValue) {
      throw new Error(`Couldn't find option value with title "${optionValueTitle}"`)
    }

    optionValue.title = newOptionValueTitle

    await magento.put(`products/options/${option.option_id}`, { option })
  })

export default program
