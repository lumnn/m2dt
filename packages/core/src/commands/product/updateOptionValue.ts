import { M2DTCommand } from "../../commander/M2DTCommand"

const program = new M2DTCommand()

program
  .description("")
  .arguments('<productSku> <optionTitle> <optionValueTitle>')
  .option('-p, --price <price>', 'New Price')
  .option('-t, --title <title>', 'New Title')
  .option('-o, --sort <sort>', 'New Sort Order')
  .option('-s, --sku <sku>', 'New Option SKU')
  .action(async (productSku: string, optionTitle: string, optionValueTitle: string, program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()

    const product = await magento.get(`products/${encodeURIComponent(productSku)}`, {
      params: {
        fields: 'sku,options'
      }
    })

    const option = product.options.find(option => option.title.trim() === optionTitle.trim())

    if (!option) {
      throw new Error(`Couldn't find option with title "${optionTitle}"`)
    }

    if (!option.values) {
      throw new Error(`API returned no option values`)
    }

    const optionValue = option.values.find(optionValue => optionValue.title.trim() === optionValueTitle.trim())

    if (!optionValue) {
      throw new Error(`Couldn't find option value with title "${optionValueTitle}"`)
    }

    if (program.price) {
      optionValue.price = program.price
    }

    if (program.title) {
      optionValue.title = program.title.trim()
    }

    if (program.sort) {
      optionValue.sort_order = program.sort
    }

    if (program.sku) {
      optionValue.sku = program.sku
    }

    await magento.put(`products/options/${option.option_id}`, { option })

  })

export default program
