import { M2DTCommand } from "../../commander/M2DTCommand"
import { attributesToObject } from "../../magento/product/attributesToObject"

const program = new M2DTCommand()

program
  .description("Deletes configurable attribute from configurable product, but re-assigns again the configurable variants. Probably good to test correct behaviour locally before using")
  .arguments('<sku> <option_id>')
  .action(async (sku: string, optionId: string, program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()

    const safeSku = encodeURIComponent(sku)

    const children = await magento.get(`configurable-products/${safeSku}/children`)

    await magento.delete(`configurable-products/${safeSku}/options/${optionId}`)

    for (const child of children) {
      await magento.post(`configurable-products/${safeSku}/child`, {
        childSku: child.sku
      })
    }
  })

export default program
