import { Command } from 'commander'
import bigLog from '../../util/bigLog'

const program = new Command()

export default program
  .description("Adds new option to attribute")
  .arguments('<attributeCode> <optionLabels...>')
  .action(addOptionAction)

async function addOptionAction (attributeCode: string, optionLabels: string|string[], program) {
  const magento = await program.parent.getMagentoInstance()
  var attribute = await magento.get(`products/attributes/${attributeCode}`)

  let isSingle = false

  if (typeof optionLabels === 'string') {
    isSingle = true
    optionLabels = [optionLabels]
  }

  var existingLabels = optionLabels.filter(label => {
    return attribute.options.find(option => option.label.toLowerCase().trim() === label.toLowerCase().trim())
  })

  if (existingLabels.length > 0) {
    throw new Error(`Theese options already exists: ${existingLabels.join(", ")}`)
  }

  for (var i = 0; i < optionLabels.length; i++) {
    var label = optionLabels[i]

    await magento.post(`products/attributes/${attributeCode}/options`, {
      option: {
        label
      }
    })
  }

  attribute = await magento.get(`products/attributes/${attributeCode}`)

  bigLog("Successfully added options")

  if (process.env.VERBOSE) {
    console.log(JSON.stringify(attribute, null, 2))
  }

  var updated = attribute.options.filter(option => optionLabels.includes(option.label))

  return isSingle ? updated[0] : updated
}

export {
  addOptionAction,
  program as addOption
}
