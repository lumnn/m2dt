import { Command } from 'commander'
import bigLog from '../../util/bigLog'

const program = new Command()

export default program
  .description("Remove option from attribute")
  .arguments('<attributeCode> <optionLabel>')
  .action(removeOptionAction)

async function removeOptionAction (attributeCode: string, optionLabel: string, program) {
  const magento = await program.parent.getMagentoInstance()
  var attribute = await magento.get(`products/attributes/${attributeCode}`, {
    fields: 'attribute_id,frontend_label,options'
  })

  var optionToRemove = attribute.options.find(option => option.label.toLowerCase().trim() === optionLabel.toLowerCase().trim())

  if (!optionToRemove) {
    throw new Error(`Couldn't find option value "${optionLabel}"`)
  }

  var result = await magento.delete(`products/attributes/${attributeCode}/options/${optionToRemove.value}`)

  bigLog("Successfully removed option")

  if (process.env.VERBOSE) {
    console.log(JSON.stringify(result, null, 2))
  }
}

export {
  removeOptionAction,
  program as removeOption
}
