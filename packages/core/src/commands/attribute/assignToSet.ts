import { Command } from 'commander'
import MagentoApi from 'magento2-api-wrapper'
import type { FilterGroup } from '../../magento/filter/filters'

const program = new Command()

program
  .description('Assigns attribute to a group')
  .arguments('<attributeCode> [setName] [groupName]')
  .action(async (attributeCode: string, setName: string, groupName: string, program) => {
    const magento = await program.parent.getMagentoInstance()
    await magento.get(`products/attributes/${attributeCode}`)

    var sets = await findSet(magento, setName)

    for (let set of sets) {
      console.log(`Adding to ${set.attribute_set_name}`)

      var filters = [{
        field: 'attribute_set_id',
        condition_type: 'eq',
        value: set.attribute_set_id
      }]

      if (groupName) {
        filters.push({
          field: 'attribute_group_name',
          condition_type: 'eq',
          value: groupName
        })
      }

      var groups = await magento.get('products/attribute-sets/groups/list', {
        params: {
          fields: "items,total_count",
          searchCriteria: {
            pageSize: 20,
            currentPage: 1,
            filterGroups: [{
              filters
            }]
          }
        }
      })

      var group

      if (groups.total_count === 0) {
        throw new Error(`Found ${groups.total_count} groups. Expected at least 1`)
      } else if (groups.total_count > 1) {
        var inquirer = require('inquirer')

        var groupQuestion = await inquirer.prompt({
          type: 'list',
          name: 'group',
          message: "Select a group to add attribute to",
          choices: groups.items.map(group => {
            return {
              name: group.attribute_group_name,
              value: group,
            }
          })
        })

        group = groupQuestion.group
      } else {
        group = groups.items[0]
      }

      var assignment = await magento.post(
        'products/attribute-sets/attributes',
        {
          attributeSetId: set.attribute_set_id,
          attributeGroupId: group.attribute_group_id,
          attributeCode,
          sortOrder: 0
        }
      )

      console.log(assignment)
    }
  })

export default program

async function findSet(magento: MagentoApi, search?: string) {
  const filterGroups: FilterGroup[] = []
  const allowInteractive = process.stdout.isTTY

  if (search) {
    filterGroups.push({
      filters: [
        {
          field: 'attribute_set_name',
          condition_type: 'like',
          value: allowInteractive ? `%${search}%` : search
        },
        {
          field: 'attribute_set_id',
          condition_type: 'eq',
          value: search
        }
      ]
    })
  }

  var sets: any = await magento.get(`products/attribute-sets/sets/list`, {
    params: {
      fields: "items[attribute_set_id,attribute_set_name],total_count",
      searchCriteria: {
        currentPage: 1,
        filterGroups,
      }
    }
  })

  if (sets.total_count === 1) {
    var set = sets.items

    console.log(`Using set "${set.attribute_set_name}" with id "${set.attribute_set_id}"`)
    return set
  }

  if (sets.total_count === 0) {
    throw new Error(`Could not find set maching "${search}"`)
  }

  var inquirer = await import('inquirer')

  var setQuestion = await inquirer.prompt([
    {
      type: 'checkbox',
      name: 'value',
      message: 'Select which set you want to add the attribute to',
      choices: sets.items.map(set => {
        return {
          name: `#${set.attribute_set_id} - ${set.attribute_set_name}`,
          value: set
        }
      })
    }
  ])

  return setQuestion.value
}
