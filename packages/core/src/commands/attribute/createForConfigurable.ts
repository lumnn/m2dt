import { Command } from 'commander'
import inquirer from 'inquirer'
import chalk from 'chalk'
import bigLog from '../../util/bigLog'
import axios from 'axios'

const program = new Command()

export default program
  .description("Creates an attribute that can be used with configurable products")
  .arguments('[attributeCode]')
  .action(createForConfigurableAction)

async function createForConfigurableAction (attributeCode: string|null, program) {
  console.log(chalk.red("Due to API limitations this command can only create dropdown option."))
  console.log(chalk.cyan("However, you'll be able to change it's type to text swatch or visual swatch in Admin panel"))
  console.log()

  const magento = await program.parent.getMagentoInstance()

  const attribute = await createAttributeForConfigurable(magento, attributeCode)

  console.log(attribute)
  console.log(chalk.bgRed("Please review above attribute."))

  const { accepted } = await inquirer.prompt([{ type: 'confirm', name: 'accepted', message: 'Is all above correct?' }])

  if (!accepted) {
    console.log("Stopped. Nothing was saved")

    return null
  }

  const savedData = await magento.put(`products/attributes/${attribute.attribute_code}`, { attribute })

  bigLog("Successfully saved attribute")

  if (process.env.VERBOSE) {
    console.log(JSON.stringify(savedData, null, 2))
  }

  return savedData
}

interface CreateAttributeForConfigurableOptions {
  addOptions: boolean,
  defaults?: any,
}

export async function createAttributeForConfigurable(magento, attributeCode: string|null, options?: CreateAttributeForConfigurableOptions) {
  options = Object.assign({
    addOptions: true,
    defaults: {},
  }, options || {})

  if (!attributeCode) {
    do {
      var codeQuestion = await inquirer.prompt([
        {
          type: 'input',
          name: 'attributeCode',
          message: 'Attribute Code',
          default: options.defaults.attribute_code,
          validate: value => value.match(/^[a-z0-9\_]+$/) ? true : 'This must contain only lower case letters, numbers and underscore'
        }
      ])

      if (!await attributeExists(codeQuestion.attributeCode, magento)) {
        attributeCode = codeQuestion.attributeCode
        break
      }

      console.log(chalk.red(`Attribute with code ${codeQuestion.attributeCode} already exists!`))
    } while (true)
  } else {
    if (await attributeExists(attributeCode, magento)) {
      throw new Error(`Attribute with code ${attributeCode} already exists!`)
    }
  }

  var attribute = await inquirer.prompt([
    {
      type: "input",
      name: "default_frontend_label",
      default: options.defaults.default_frontend_label,
      message: "Default frontend label"
    }
  ])

  Object.assign(attribute, {
    attribute_code: attributeCode,
    scope: "global",
    frontend_input: "select",
  })

  if (options.addOptions) {
    attribute.options = []

    do {
      var option = await inquirer.prompt([
        {
          type: "input",
          name: "label",
          message: "Option Value (leave empty to stop adding values)"
        }
      ])

      if (option.label) {
        attribute.options.push(option)
      }
    } while (option.label)
  }

  return attribute
}

async function attributeExists(attributeCode: string, magento): Promise<boolean|undefined> {
  try {
    var currentAttribute = await magento.get(`products/attributes/${attributeCode}`)
  } catch (e) {
    if (axios.isAxiosError(e) && e.response && e.response.status === 404) {
      return false
    }

    throw e
  }

  return true
}

export {
  createForConfigurableAction,
  program as createForConfigurable
}
