const { Command } = require('commander')
const path = require('path')
const getMagentoInstance = require('../util/getMagentoInstance').default

const program = new Command()
var scriptName = path.basename(__filename).replace('.js', '')
scriptName = scriptName.replace(/([A-Z])/g, letter => `:${letter.toLowerCase()}`)

program
  .name(scriptName)
  .arguments('<path>')
  .description("Allows to quickly execut GET request on API")
  .option('-r, --raw', 'Output in plain JSON')
  .option('-B, --reset-base-url', 'Resets base url to exclude /rest/V1 part')
  .action(async (path, program) => {
    const magento = await program.parent.getMagentoInstance()

    var params = {}

    if (program.resetBaseUrl) {
      params.baseURL = magento.apiParams.url
    }

    var result = await magento.get(`${path}`, { params, storeCode: program.parent.store })

    if (program.raw) {
      console.log(JSON.stringify(result, null, 2))

      return
    }

    console.log(result)
  })

module.exports = program
