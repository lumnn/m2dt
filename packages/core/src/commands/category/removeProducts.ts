import { M2DTCommand } from "../../commander/M2DTCommand"
import { parseMessage } from "../../magento/error/parseMessage"
import { attributesToObject } from "../../magento/product/attributesToObject"

const program = new M2DTCommand()

program
  .description("Removes one or more products from category")
  .arguments('<categoryId> <productSkus...>')
  .action(async (categoryId: string, productSkus: string[], program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()

    const category = await magento.get(`categories/${categoryId}`)

    for (const sku of productSkus) {
      var result

      const safeSku = encodeURIComponent(sku)

      try {
        result = await magento.delete(`categories/${categoryId}/products/${safeSku}`)
      } catch (e) {
        result = findExceptionMessage(e)
      }

      console.log(sku, result)
    }
  })

function findExceptionMessage (error) {
  if (error.response.data.message) {
    return parseMessage(error.response.data)
  }

  if (error.response.data) {
    return error.response.data
  }

  return error.response.status + ' ' + error.message
}

export default program
