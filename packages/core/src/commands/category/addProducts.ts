import { M2DTCommand } from "../../commander/M2DTCommand"
import { parseMessage } from "../../magento/error/parseMessage"
import { attributesToObject } from "../../magento/product/attributesToObject"

const program = new M2DTCommand()

program
  .description("Assigns one or more products to category")
  .arguments('<categoryId> <productSkus...>')
  .action(async (categoryId: string, productSkus: string[], program: M2DTCommand) => {
    const magento = await program.getMagentoInstance()

    const category = await magento.get(`categories/${categoryId}`)

    for (const sku of productSkus) {
      var result

      try {
        result = await magento.post(`categories/${categoryId}/products`, {
          productLink: {
            category_id: categoryId,
            sku
          }
        })
      } catch (e) {
        result = findExceptionMessage(e)
      }

      console.log(sku, result)
    }
  })

function findExceptionMessage (error) {
  if (error.response.data.message) {
    return parseMessage(error.response.data)
  }

  if (error.response.data) {
    return error.response.data
  }

  return error.response.status + ' ' + error.message
}

export default program
