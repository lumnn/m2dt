import { M2DTCommand } from '../commander/M2DTCommand'
import chalk from 'chalk'
import { diffWordsWithSpace } from 'diff'
import { getUpdateDiff, beforeCompare } from '../util/diff'
import { ObjectPrinter } from '../compare/output/ObjectPrinter'

const program = new M2DTCommand()

program
  .description("Compare input data with Magento API data. Output is a minimal JSON diff that would make Magento data same as input")
  .arguments('<resource> [id]')
  .option('--output <type>', 'Controls what output style should be used. Defaults to JSON', 'json')
  .action(async function (this: M2DTCommand, resource, id, { output = 'json' }) {
    const { readStdinOnce } = require('../util/readStdinOnce')
    const magento = await this.getMagentoInstance()
    const resourceConfig = await this.loadResourceConfig(resource)

    var getUrl = resourceConfig.endpoints && resourceConfig.endpoints.get
      ? resourceConfig.endpoints.get
      : resourceConfig.name

    var inputText = await readStdinOnce()
    var input = JSON.parse(inputText)

    if (!id) {
      id = input[resourceConfig.idProp]
    }

    id = encodeURIComponent(id)

    var result = await magento.get(`${getUrl}/${id}`, {
      storeCode: this.root.store,
    })

    var diff = getUpdateDiff(input, result, resourceConfig.compareOptions)

    if (this.config.resources && this.config.resources[resource] && this.config.resources[resource].afterCompare) {
      var configAfterCompare = this.config.resources[resource].afterCompare
      diff = configAfterCompare(diff, input, result)
    }

    if (!diff) {
      return
    }

    diff[resourceConfig.idProp] = result[resourceConfig.idProp]

    if (output === 'json') {
      console.log(JSON.stringify(diff, null, 2))
      return
    }

    if (output === 'table') {
      const { printerFactory } = require('../compare/output/product/productPrinterFactory')
      var printer = printerFactory()
      printer.print(result, diff, { compareOptions: resourceConfig.compareOptions })

      // printTableDiff(diff, input, result, resourceConfig.compareOptions)
      return
    }

    throw new Error(`Unknown output format ${output}`)
  })

interface PrintTableDiffOptions {
  keyPrefix: string
}

function printTableDiff(objectDiff, input, orig, resourceConfig, options: PrintTableDiffOptions = { keyPrefix: '' }) {
  objectDiff = beforeCompare(objectDiff, resourceConfig)
  orig = orig && beforeCompare(orig, resourceConfig)

  for (var key of Object.keys(objectDiff)) {
    if (orig && objectDiff[key] === orig[key]) {
      // there is for example produc SKU that will always be present even if it's not changed
      continue
    }

    var keyOptions = (resourceConfig.children && resourceConfig.children[key]) || {}

    if (typeof objectDiff[key] === 'object') {
      printTableDiff(objectDiff[key], null, orig[key], keyOptions, Object.assign({}, options, { keyPrefix: options.keyPrefix + key + '.' }))
      continue
    }

    console.log()
    console.log(chalk.bold.bgBlue(options.keyPrefix + key))

    if (!orig || !orig[key]) {
      console.log(chalk.green(objectDiff[key]))
      continue
    } else if (orig[key] && !objectDiff[key]) {
      console.log(chalk.red(orig[key]))
      continue
    }

    var propDiff = diffWordsWithSpace(orig[key].toString(), objectDiff[key].toString())

    if (orig[key].toString().length <= process.stdout.columns && objectDiff[key].toString().length <= process.stdout.columns) {
      console.log(chalk.red(orig[key]))
      console.log(chalk.green(objectDiff[key]))

      if (propDiff.length === 2) {
        continue
      }
    }

    for (var diffPart of propDiff) {
      var color = chalk.reset

      if (diffPart.added) {
        color = chalk.green.bold
      } else if (diffPart.removed) {
        color = chalk.gray
      }

      process.stdout.write(color(diffPart.value))
    }

    console.log()
  }
}

export {
  program as default,
  program as compare
}
