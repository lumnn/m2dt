import { M2DTCommand } from '../commander/M2DTCommand'
import path from 'path'
const scriptName = path.basename(__filename).replace('.js', '')

const program = new M2DTCommand()

program
  .name(scriptName)
  .description("Update resource")
  .arguments('<resource> <id>')
  .action(async function(this: M2DTCommand, resource, id, program) {
    const { readStdinOnce } = require('../util/readStdinOnce')
    const chalk = require('chalk')
    const normalizeResourceName = require('../magento/normalizeResourceName').default

    const magento = await program.parent.getMagentoInstance()

    resource = normalizeResourceName(resource)
    if (process.env.VERBOSE) {
      console.log(chalk.blue(`Using resource "${resource}"`))
    }

    var input = await readStdinOnce()
    input = JSON.parse(input)

    if (!input) {
      throw new Error("Misisng input")
    }

    const resourceConfig = await this.loadResourceConfig(resource)

    var basePath = resourceConfig.endpoints && resourceConfig.endpoints.base
      ? resourceConfig.endpoints.base
      : resource

    var singularName = resourceConfig.singularName || resource

    var saveData = {}
    saveData[singularName] = input

    const safeId = encodeURIComponent(id)

    var result = await magento.put(
      `${basePath}/${safeId}`,
      saveData,
      { storeCode: program.parent.store }
    )

    // after plugin?

    if (program.raw) {
      console.log(JSON.stringify(result, null, 2))

      return
    }

    console.log(result)
  })

module.exports = program
