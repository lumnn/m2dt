module.exports = function setupFieldsOption (program) {
  program
    .option(
      '-F, --fields <fields...>',
      'Filter Magento output (see: https://devdocs.magento.com/guides/v2.4/rest/retrieve-filtered-responses.html)',
      function (value, accumulated) {
        if (process.env.VERBOSE) {
          console.log("Option Fields: " + value)
        }

        if (accumulated === true) {
          return value
        }

        return accumulated + ',' + value
      }
    )
}
