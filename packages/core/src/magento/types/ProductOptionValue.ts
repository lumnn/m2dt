export interface ProductOptionValue {
  option_type_id?: number,
  title?: string,
  price?: number,
  price_type?: string,
  sort_order?: number,
}

export function isProductOptionValue(arg: any): arg is ProductOptionValue {
  if (
    typeof arg === 'object'
    && ['undefined', 'number'].includes(typeof arg.option_type_id)
    && ['undefined', 'string'].includes(typeof arg.title)
    && ['undefined', 'number'].includes(typeof arg.price)
    && ['undefined', 'string'].includes(typeof arg.price_type)
    && ['undefined', 'number'].includes(typeof arg.sort_order)
  ) {
    return true
  }

  return false
}
