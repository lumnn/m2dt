export interface AttributeOption {
  label: string,
  value?: string
}

export function isAttributeOption(arg: any): arg is AttributeOption {
  if (
    typeof arg === 'object'
    && typeof arg.label === 'string'
    && (arg.value === undefined || typeof arg.value === 'string')
  ) {
    return true
  }

  return false
}
