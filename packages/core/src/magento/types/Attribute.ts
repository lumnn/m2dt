import { AttributeOption, isAttributeOption } from './AttributeOption'

export interface Attribute {
  attribute_code: string,
  attribute_id?: number,
  options?: AttributeOption[],
  frontend_input?: string,
  default_frontend_label?: string,
  scope?: string,
  is_required?: boolean,
}

export function isAttribute(arg: any): arg is Attribute {
  if (
    typeof arg === 'object'
    && typeof arg.attribute_code === 'string'
    && ['undefined', 'number'].includes(typeof arg.attribute_id)
    && (arg.options === undefined || (Array.isArray(arg.options) && arg.options.every(isAttributeOption)))
    && ['undefined', 'string'].includes(typeof arg.frontend_input)
    && ['undefined', 'string'].includes(typeof arg.default_frontend_label)
    && ['undefined', 'string'].includes(typeof arg.scope)
    && ['undefined', 'string'].includes(typeof arg.is_required)
  ) {
    return true
  }

  return false
}
