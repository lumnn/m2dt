export interface ProductAttribute {
  attribute_code: string,
  value: string|number|null
}
