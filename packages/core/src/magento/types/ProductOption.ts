import { ProductOptionValue, isProductOptionValue } from './ProductOptionValue'

export interface ProductOption {
  option_id?: number,
  product_sku?: string,
  title?: string,
  type?: string,
  values?: ProductOptionValue[],
  sort_order?: number,
  is_require?: boolean,
}

export function isProductOption(arg: any): arg is ProductOption
{
  if (
    typeof arg === 'object'
    && ['undefined', 'number'].includes(typeof arg.option_id)
    && ['undefined', 'string'].includes(typeof arg.product_sku)
    && ['undefined', 'string'].includes(typeof arg.title)
    && ['undefined', 'string'].includes(typeof arg.type)
    && (arg.values === undefined || (Array.isArray(arg.values) && arg.options.every(isProductOptionValue)))
    && ['undefined', 'number'].includes(typeof arg.sort_order)
    && ['undefined', 'boolean'].includes(typeof arg.sort_order)
  ) {
    return true
  }

  return false
}
