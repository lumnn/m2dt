import { ProductAttribute } from "../types/ProductAttribute";
import { ProductAttributesObject } from "./ProductAttributeObject";

export function attributesFromObject (attributesObject: ProductAttributesObject): ProductAttribute[] {
  const array: ProductAttribute[] = []

  for (let attributeCode in attributesObject) {
    array.push({
      attribute_code: attributeCode,
      value: attributesObject[attributeCode]
    })
  }

  return array
}
