export interface ProductAttributesObject {
  [attributeCode: string]: string|number
}
