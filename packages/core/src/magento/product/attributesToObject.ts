import { ProductAttribute } from "../types/ProductAttribute";
import { ProductAttributesObject } from "./ProductAttributeObject";

export function attributesToObject (attributesArray: ProductAttribute[]): ProductAttributesObject {
  const object: ProductAttributesObject = {}

  for (let attribute of attributesArray) {
    if (!attribute.value) {
      continue
    }

    object[attribute.attribute_code] = attribute.value
  }

  return object
}
