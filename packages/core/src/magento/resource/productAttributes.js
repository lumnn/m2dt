module.exports = {
  idProp: 'attribute_code',
  singularName: 'attribute',
  endpoints: {
    base: 'products/attributes',
    list: 'products/attributes',
    get: 'products/attributes',
  },
  columns: [
    "attribute_id",
    "attribute_code",
    "default_frontend_label",
    "scope",
    "frontend_input"
  ]
}
