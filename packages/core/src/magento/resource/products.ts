import { getArrayDiff } from '../../util/diff'
import { ResourceConfigInterface } from './ResourceConfigInterface'
import { attributesToObject } from '../product/attributesToObject'

const resourceConfig: ResourceConfigInterface = {
  name: 'products',
  singularName: 'product',
  idProp: 'sku',

  endpoints: {
    base: 'products'
  },

  columns: [
    "id",
    "sku",
    "name",
    "price",
    'type_id'
  ],

  compareOptions: {
    beforeCompare(product) {
      var product = {...product}
      // updated at should not be compared as it's a value automatically changed
      delete product.updated_at

      return product
    },
    children: {
      extension_attributes: {
        children: {
          category_links: {
            // convert to object
            beforeCompare(data) {
              var attrs = {}
              data.forEach(catLink => attrs[catLink.category_id] = catLink.position)
              return attrs
            },
            // and back to array
            afterCompare(data) {
              return Object.keys(data)
              .map(key => ({
                position: data[key],
                category_id: key,
              }))
            }
          },
          website_ids: {
            missingIsRemoved: true,
          }
        }
      },
      custom_attributes: {
        // convert to object
        beforeCompare(data) {
          var attrs: any = {}
          data.forEach(mageAttribute => attrs[mageAttribute.attribute_code] = mageAttribute.value.toString())

          if (attrs.url_key) {
            attrs.url_key = attrs.url_key.toLowerCase()
          }

          return attrs
        },
        // and back to array
        afterCompare(diff, input, original) {
          return Object.keys(diff)
            // filter the attributes that are empty and are missing in Magento
            .filter(key => diff[key] !== '' || original[key] !== undefined)
            .map(key => ({
              attribute_code: key,
              value: diff[key],
            }))
        }
      },
      options: {
        beforeCompare (options) {
          return options.map(option => {
            option = {...option}
            return option
          })
        },
        arrayCompareCheckOrder: false,
        arrayValueCompare (a, b) {
          if (a.title !== b.title
            || a.type !== b.type
            || a.sort_order !== b.sort_order
            || a.is_require !== b.is_require
            || a.values.length !== b.values.length
            || (a.option_id !== undefined && b.option_id !== undefined && a.option_id !== b.option_id)
          ) {
            return false
          }

          return !getArrayDiff(a.values, b.values, {
            arrayCompareCheckOrder: false,
            arrayValueCompare (a, b) {
              return a.title === b.title
                && a.price === b.price
                && a.price_type === b.price_type
            }
          })
        },
      },
      tier_prices: {
        // update price to be number
        beforeCompare(prices) {
          return prices.map(tierPrice => {
            return {
              ...tierPrice,
              value: Number.parseFloat(tierPrice.value)
            }
          })
        },
        // revert price to be fixed with 4 digits
        afterCompare(prices) {
          return prices.map(tierPrice => {
            return {
              ...tierPrice,
              value: tierPrice.value.toFixed(4)
            }
          })
        },
        arrayValueCompare(a, b) {
          return a.customer_group_id === b.customer_group_id
          && a.qty === b.qty
          && a.value - b.value < 0.0000001
        }
      },
    },
    afterCompare(diff, update, existing) {
      if (update.type_id === 'simple' && existing.type_id === 'configurable') {
        delete diff.price
      }

      return diff
    },
  },

  prettify (object) {
    if (object.custom_attributes) {
      const sortedAttributes = object.custom_attributes.sort((a, b) => a.attribute_code.localeCompare(b.attribute_code))
      object.attributes = attributesToObject(sortedAttributes)

      delete object.custom_attributes
    }

    if (object.options.length === 0) {
      delete object.options
    }

    if (object.tier_prices.length === 0) {
      delete object.tier_prices
    }

    if (object.product_links.length === 0) {
      delete object.product_links
    }

    return object
  }
}

export {
  resourceConfig as default,
  resourceConfig
}
