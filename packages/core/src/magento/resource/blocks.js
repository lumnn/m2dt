module.exports = {
  endpoints: {
    base: 'cmsBlock',
    list: 'cmsBlock/search',
  },
  columns: [
    "id",
    "identifier",
    "title"
  ]
}
