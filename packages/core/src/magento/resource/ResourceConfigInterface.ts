import { CompareOptions } from "../../compare/CompareOptions";

export interface ResourceConfigInterface {
  name: string,
  singularName: string,
  idProp: string,
  columns: string[],
  compareOptions?: CompareOptions,
  endpoints?: ResourceConfigEndpoints,
  prettify?: ResourcePrettifyFunction,
}

interface ResourcePrettifyFunction {
  (object: any): any;
}

interface ResourceConfigEndpoints {
  base?: string,
  list?: string,
  get?: string,
}
