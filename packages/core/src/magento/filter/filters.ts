export interface FilterGroup {
  filters: Filter[]
}

export interface Filter {
  field: string,
  condition_type: string,
  value: string|number
}
