export function parseMessage (error) {
  if (!error.message) {
    throw new Error("Invalid data passed. Expected object with 'message' key")
  }

  if (!error.parameters) {
    return error.message
  }

  var message = error.message
  var parameterKeys = Object.keys(error.parameters)

  for (let i = 0; i < parameterKeys.length; i++) {
    let parameter = parameterKeys[i]
    message = message
      .replace(`%${parameter}`, error.parameters[parameter])
      .replace(`%${i+1}`, error.parameters[parameter])
  }

  return message
}

export default parseMessage
