import { singular } from '../resources/magento/resourceAliases.json'

export default function normalizeResourceName (resource: string): string {
  resource = resource.replace(/[^a-z0-9\-\_]/ig, '')

  if (singular[resource]) {
    resource = singular[resource]
  }

  return resource
}

export { normalizeResourceName }
