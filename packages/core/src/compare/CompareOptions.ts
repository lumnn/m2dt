export interface CompareOptions {
  beforeCompare?: BeforeCompare,
  afterCompare?: AfterCompare,
  children?: CompareOptionsChildren,
  allChildren?: CompareOptions,
  arrayCompareCheckOrder?: boolean,
  arrayValueCompare?: ArrayValueCompare,
  // whether it should treat missing values as removed (true) or unchanged (false - default)
  missingIsRemoved?: boolean,
}

export interface CompareOptionsChildren {
  [key: string]: CompareOptions
}

interface BeforeCompare {
  (object: any): any
}

interface AfterCompare {
  (diff: any, input: any, original: any): any
}

interface ArrayValueCompare {
  (a: any, b: any): boolean
}
