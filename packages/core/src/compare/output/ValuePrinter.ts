import chalk from 'chalk'
import { diffWordsWithSpace } from 'diff'

export class ValuePrinter {
  print (origValue, diffValue) {
    if (!origValue) {
      console.log(chalk.green(diffValue))
      return
    } else if (origValue && !diffValue) {
      console.log(chalk.red(origValue))
      return
    }

    var propDiff = diffWordsWithSpace(origValue.toString(), diffValue.toString())

    if (origValue.toString().length <= process.stdout.columns && diffValue.toString().length <= process.stdout.columns) {
      console.log(chalk.red(origValue))
      console.log(chalk.green(diffValue))

      if (propDiff.length === 2) {
        return
      }
    }

    for (var diffPart of propDiff) {
      var color = chalk.reset

      if (diffPart.added) {
        color = chalk.green.bold
      } else if (diffPart.removed) {
        color = chalk.gray
      }

      process.stdout.write(color(diffPart.value))
    }

    console.log()
  }
}
