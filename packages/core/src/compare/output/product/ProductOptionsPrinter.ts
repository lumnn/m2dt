import chalk from "chalk"
import { ObjectPrinter } from "../ObjectPrinter"
import { PrintTableDiffContext } from "../PrinterInterface"

export class ProductOptionsPrinter {
  objectPrinter: ObjectPrinter

  constructor () {
    this.objectPrinter = new ObjectPrinter()
  }

  print (orig, diff, context?: PrintTableDiffContext) {
    orig = [...orig]
    diff = [...diff]

    var removeOptions: any = []
    var existingOptions: any = []

    for (var origOption of orig) {
      var diffOptionIndex = diff.findIndex(diffOption => diffOption.title === origOption.title)
      var diffOption = diff[diffOptionIndex]
      diff.splice(diffOptionIndex, 1)

      if (diffOption) {
        existingOptions.push({
          diff: diffOption,
          orig: origOption
        })

        continue
      }

      removeOptions.push(origOption)
    }

    if (diff.length > 0) {
      console.log("New Options:")

      diff.forEach(option => console.log(chalk.green(option.title)))
      console.log()
    }

    if (removeOptions.length > 0) {
      console.log("Options to Remove:")

      removeOptions.forEach(option => console.log(chalk.red(option.title)))
      console.log()
    }

    if (existingOptions.length > 0) {
      console.log("Options to be Changed:")

      existingOptions.forEach(option => {
        console.log(chalk.bold(option.diff.title))

        this.objectPrinter.print(option.orig, option.diff, {
          keyPrefix: 'option'
        })
      })
    }
  }
}
