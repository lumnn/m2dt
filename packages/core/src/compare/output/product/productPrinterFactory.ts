import { ResourceConfigInterface } from "../../../magento/resource/ResourceConfigInterface";
import { ObjectPrinter } from "../ObjectPrinter";
import { ProductOptionsPrinter } from "./ProductOptionsPrinter";

export function printerFactory(resourceConfig: ResourceConfigInterface) {
  return new ObjectPrinter({
    printersByKey: {
      'options': new ProductOptionsPrinter()
    },
  })
}
