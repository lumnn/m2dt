import { CompareOptions } from "../CompareOptions";

export interface PrinterInterface {
  print(orig: any, diff: any, context?: PrintTableDiffContext): void
}

export interface PrintTableDiffContext {
  keyPrefix?: string,
  compareOptions?: CompareOptions|undefined
}
