import chalk from 'chalk'
import { beforeCompare } from '../../util/diff'
import { CompareOptions } from '../CompareOptions'
import { PrinterInterface, PrintTableDiffContext } from './PrinterInterface'
import { ValuePrinter } from './ValuePrinter'

interface PrinterCollection {
  [key: string]: PrinterInterface
}

interface ObjectPrinterConstructorOptions {
  printersByType?: PrinterCollection
  printersByKey?: PrinterCollection
  defaultValuePrinter?: PrinterInterface
  compareOptions?: undefined|CompareOptions,
  withKeyPrint?: boolean,
}

export class ObjectPrinter {
  printersByType: PrinterCollection
  printersByKey: PrinterCollection
  defaultValuePrinter: PrinterInterface

  constructor (options: ObjectPrinterConstructorOptions = {}) {
    this.printersByType = options.printersByType || {}
    if (!this.printersByType['object']) {
      this.printersByType['object'] = this
      this.printersByType['array'] = this
    }

    this.printersByKey = options.printersByKey || {}
    this.defaultValuePrinter = options.defaultValuePrinter || new ValuePrinter()
  }

  print (
    orig: any,
    diff: any,
    context?: PrintTableDiffContext
  ) {
    orig = this.beforeCompare(orig, context)
    diff = this.beforeCompare(diff, context)

    if (context && context.keyPrefix) {
      var printer = this.printersByKey[context.keyPrefix]

      if (printer) {
        return printer.print(orig, diff, context)
      }
    }

    var keys: string[] = []

    if (diff) {
      keys.push(...Object.keys(diff))
    }

    if ((!diff && orig) || (context && context.compareOptions && context.compareOptions.missingIsRemoved === true)) {
      for (var key of Object.keys(orig)) {
        if (!keys.includes(key)) {
          keys.push(key)
        }
      }
    }

    for (var key of keys) {
      if (orig && diff && diff[key] === orig[key]) {
        // there is for example produc SKU that will always be present even if it's not changed
        continue
      }

      var keyDiff = diff && diff[key]
      var keyOrig = orig && orig[key]

      var type = Array.isArray(keyDiff) ? 'array' : typeof keyDiff
      var printer = this.printersByKey[key] || this.printersByType[type]

      if (printer !== this) {
        this.printKey(key, context)
      }

      var keyContext = this.createKeyContext(key, context)

      if (printer) {
        printer.print(keyOrig, keyDiff, keyContext)
        continue
      }

      this.defaultValuePrinter.print(keyOrig, keyDiff, keyContext)
      console.log()
    }
  }

  printKey (key: string, context?: PrintTableDiffContext): void {
    if (context && context.keyPrefix) {
      console.log(chalk.bold.bgBlue([context.keyPrefix, key].join('.')))
      return
    }

    console.log(chalk.bold.bgBlue(key))
  }

  createKeyContext (key: string, context?: PrintTableDiffContext): PrintTableDiffContext {
    return {
      keyPrefix: [context && context.keyPrefix, key]
        .filter(a => a)
        .join('.'),
      compareOptions: context && context.compareOptions && context.compareOptions.children && context.compareOptions.children[key]
        ? context.compareOptions.children[key]
        : undefined
    }
  }

  beforeCompare (object, context?: PrintTableDiffContext) {
    return beforeCompare(object, context && context.compareOptions)
  }
}
