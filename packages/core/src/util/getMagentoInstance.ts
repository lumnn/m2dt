import MagentoApi from 'magento2-api-wrapper'

export function getMagentoInstance (config, profile: string|null = null, storeCode: string = 'all'): MagentoApi {
  checkMagentoConfig(config, profile)

  var magentoConfig = profile
    ? config.magento.profiles[profile]
    : getDefaultConfig(config)

  if (!magentoConfig.client) {
    magentoConfig.client = {}
  }

  if (!magentoConfig.client.axios) {
    magentoConfig.client.axios = {}
  }

  magentoConfig.client.axios.storeCode = storeCode

  return new MagentoApi(magentoConfig.client)
}

export default getMagentoInstance

function checkMagentoConfig(config, profile) {
  if (!config.magento) {
    throw new Error("Missing magento configuration")
  }

  if (!config.magento.profiles) {
    throw new Error("Missing magento 'profiles' configuration")
  }

  if (profile && !config.magento.profiles[profile]) {
    throw new Error(`Missing magento profile with name "${profile}"`)
  }
}

/**
 * Finds first config with isDefault set to true
 */
function getDefaultConfig(config) {
  var profiles = Object.values(config.magento.profiles)
  var defaultConfig = profiles.find(profile => (profile as any).isDefault)

  return defaultConfig ? defaultConfig : profiles[0]
}
