var filterRegex = /([\w.]+)\s+(=|>|>=|<|<=|n?eq|n?finset|from|gte?|gteq|n?in|n?like|lte?|lteq|moreq|to)\s+(.+)/
var filterValuelessRegex = /([\w.]+)\s+(null|notnull|nnull)/

var filterConditions = {
  "=": "eq",
  ">": "gt",
  ">=": "gte",
  "<": "lt",
  "<=": "lte",
  "nnull": "notnull"
}

/**
 * Parses string like ["sku = T-SHIRT-50;sku = T-SHIRT-25"] into a filterGroups
 * that are used by Magento
 *
 * @param  {string[]} filterGroups  The filter groups
 * @param  {string}   [splitBy=';'] The split by
 * @return {Object}   { description_of_the_return_value }
 */
module.exports = function (filterGroups, splitBy = ';') {
  return filterGroups
    .map(group => {
      var filtersParts = group.split(splitBy)

      if (!filtersParts) {
        return
      }

      var filters = filtersParts
        .map(filter => {
          var matches = filter.match(filterRegex)

          if (!matches) {
            matches = filter.match(filterValuelessRegex)
          }

          if (!matches) {
            throw new Error(`Failed to process filter string '${filter}'`)
          }

          return {
            field: matches[1],
            condition_type: filterConditions[matches[2]] || matches[2],
            value: matches[3]
          }
        })
        .filter(a => a)

      return { filters }
    })
    .filter(a => a)
}
