/**
 * Function to read input once and proceed. Ensures that stdin is paused, so
 * program won't stuck
 */
export function readStdinOnce (): Promise<string> {
  return new Promise(resolve => {
    var input = ''

    process.stdin.on('data', chunk => {
      input += chunk.toString()
    })

    process.stdin.on('end', () => {
      // process.stdin.pause()
      resolve(input)
    })
  })
}

export default readStdinOnce
