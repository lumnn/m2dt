import assert from 'assert'
import { CompareOptions } from '../compare/CompareOptions'

export function beforeCompare(object: object, options?: CompareOptions) {
  return options && options.beforeCompare ? options.beforeCompare(object) : clone(object)
}

export function getUpdateDiff(update: object, orig: object, options: CompareOptions = {}) {
  update = beforeCompare(update, options)
  orig = beforeCompare(orig, options)

  // both are arrays
  if (update instanceof Array && orig instanceof Array) {
    let result = getArrayDiff(update, orig, options)
    return (options.afterCompare && result) ? options.afterCompare(result, update, orig) : result
  }

  // type mismatch... just return update
  if (update instanceof Array || orig instanceof Array) {
    return update
  }

  let result = getObjectDiff(update, orig, options)
  return (options.afterCompare && result) ? options.afterCompare(result, update, orig) : result
}

export function getArrayDiff(update: any[], orig: any[], options: CompareOptions) {
  try {
    assert.deepStrictEqual(update, orig)
    return null
  } catch (e) {}

  if (options.arrayValueCompare && update.length === orig.length) {
    for (var i = 0; i < update.length; i++) {
      if (options.arrayCompareCheckOrder === false) {
        // check if current update value can be found in orig array
        if (!orig.find(options.arrayValueCompare.bind(null, update[i]))) {
          return update
        }

        continue
      }

      if (!options.arrayValueCompare(update[i], orig[i])) {
        return update
      }
    }

    return null
  }

  return update
}

export function getObjectDiff(update: object, orig: object, options: CompareOptions) {
  // remove keys from orig if it's missing in update
  for (var key in orig) {
    if (update[key] === undefined) {
      delete orig[key]
      continue
    }
  }

  // try checking deep equality, if this passes, then we're fine!
  try {
    assert.deepStrictEqual(update, orig)
    return null
  } catch (e) {}

  var diff = {}

  for (var key in update) {
    if (typeof update[key] === typeof orig[key] && typeof update[key] === 'object') {
      var keyOptions = (options.children && options.children[key]) || {}
      var keyDiff = getUpdateDiff(update[key], orig[key], keyOptions)

      if (keyDiff) {
        diff[key] = keyDiff
      }

      continue
    }

    if (update[key] !== orig[key]
      || typeof update[key] !== typeof orig[key]
    ) {
      diff[key] = update[key]
      continue
    }
  }

  return Object.keys(diff).length === 0 ? null : diff
}

function clone (value: any) {
  if (typeof value !== 'object') {
    return value
  }

  if (value instanceof Array) {
    return [...value]
  }

  return {...value}
}
