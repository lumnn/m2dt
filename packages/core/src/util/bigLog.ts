import chalk from 'chalk'

export default function bigLog(message: string, chalkColor: string = 'bgGreen') {
  console.log()
  console.log(chalk[chalkColor](`   ${" ".repeat(message.length)}   `))
  console.log(chalk[chalkColor](`   ${message}   `))
  console.log(chalk[chalkColor](`   ${" ".repeat(message.length)}   `))
  console.log()
}

export { bigLog }
