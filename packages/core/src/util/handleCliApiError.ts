import chalk from 'chalk'
import errorParseMessage from '../magento/error/parseMessage'
import { AxiosError, default as axios } from 'axios'
import type { Magento2ApiError } from 'magento2-api-wrapper'

function isMagento2ApiError(error: AxiosError | Magento2ApiError): error is Magento2ApiError {
  return "magento" in error
}

/**
 * A function that displays message about API request error
 */
export function handleCliApiError (e: AxiosError | Magento2ApiError) {
  if (!axios.isAxiosError(e)) {
    throw e
  }

  if (e.config) {
    console.log(
      chalk.bgRed("API:"),
      e.message
    )

    var method = e.config.method ? e.config.method.toUpperCase() : ''
    var base = ''

    if (e.config.baseURL) {
      var slash = e.config.baseURL[e.config.baseURL.length - 1] === '/' ? '' : '/'
      var base = e.config.baseURL + slash
    }

    var uri = isMagento2ApiError(e) ? e.magento.axios.getUri(e.config) : e.config.url
    console.log(chalk.gray(method + ' ' + base + uri))
  }

  if (e.response) {
    try {
      var errorMessage = errorParseMessage(e.response.data)
    } catch {
      errorMessage = "There was unexpected Magento error response"
    }

    console.log(`${chalk.bgRed("Magento:")} ${errorMessage}`)

    return
  }

  throw e
}

export default handleCliApiError
