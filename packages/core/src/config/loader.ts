import path from 'path'

interface ConfigLoadResult {
  path: string,
  config: any,
}

export function load (configPath?: string): ConfigLoadResult {
  configPath = configPath && configPath[0] !== '/'
    ? path.join(process.cwd(), configPath)
    : configPath

  if (configPath) {
    try {
      return loadFromPath(configPath)
    } catch (e) {
      throw new Error(`Failed to load config from "${configPath}"`)
    }
  }

  const fs = require('fs')
  var localConfigPath = path.join(process.cwd(), 'm2dt.config.js')

  if (fs.existsSync(localConfigPath)) {
    return loadFromPath(localConfigPath)
  }

  const xdg = require('@folder/xdg')
  const xdgDirs = xdg()
  const globalConfigPath = path.join(xdgDirs.config, 'm2dt.config.js')

  if (fs.existsSync(globalConfigPath)) {
    return loadFromPath(globalConfigPath)
  }

  throw new Error(`Filed to load any config. Tried ${[localConfigPath, globalConfigPath].join(', ')}. Try tunning m2dt configure`)
}

export function loadFromPath (path: string): ConfigLoadResult {
  return {
    config: require(path),
    path: path,
  }
}
