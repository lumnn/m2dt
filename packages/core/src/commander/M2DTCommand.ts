import { Command } from 'commander';
import { ResourceConfigInterface } from '../magento/resource/ResourceConfigInterface';
import { normalizeResourceName } from '../magento/normalizeResourceName';
import Magento2Api from 'magento2-api-wrapper';
import { Stderr } from '../logger/Stderr';

export class M2DTCommand extends Command {
  constructor () {
    super(...arguments)
  }

  _dispatchSubcommand() {
    if (arguments[0]) {
      var event = {
        name: arguments[0],
        operands: arguments[1]
      }

      this.emit('before:*', event)
      this.emit('before:' + event.name, event)
    }

    super._dispatchSubcommand.apply(this, arguments)
  }

  parseOptions(argv: string[]) {
    var result = super.parseOptions(argv)

    if (this._afterOptions) {
      this._afterOptions()
    }

    return result
  }

  async getMagentoInstance (): Promise<Magento2Api> {
    if (this.parent) {
      return this.root.getMagentoInstance()
    }

    if (this.magento) {
      return this.magento
    }

    const { getMagentoInstance } = await import('../util/getMagentoInstance')
    return this.magento = getMagentoInstance(this.config, this.root.profile, this.root.store || 'all')
  }

  async loadResourceConfig (resource: string): Promise<ResourceConfigInterface> {
    resource = normalizeResourceName(resource)

    if (process.env.VERBOSE) {
      console.log(`Using resource "${resource}"`)
    }

    var loader = this.config.resourceConfigLoader || defaultResourceConfigLoader
    return await loader(resource)
  }

  get root () {
    var instance = this
    while (instance.parent) {
      instance = instance.parent
    }

    return instance
  }

  get config () {
    return this.root.configObject
  }

  set config (value) {
    if (this.parent) {
      throw new Error("Cannot set config on non-root command")
    }

    this.configObject = value
  }

  get logger () {
    if (this.parent) {
      return this.root.logger
    }

    if (!this._logger) {
      this._logger = new Stderr(process.env.VERBOSE ? 8 : 4)
    }

    return this._logger
  }
}

async function defaultResourceConfigLoader(resource: string): Promise<ResourceConfigInterface> {
  var resourceConfig = require(`../magento/resource/${resource}`)
  if (resourceConfig.default) {
    return resourceConfig.default
  }

  return resourceConfig
}

export default M2DTCommand
