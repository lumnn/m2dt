import chalk from 'chalk'

export class Stderr {
  logLevel: number

  constructor (logLevel: number) {
    this.logLevel = logLevel
  }

  emergency(message: string, context: object) {
    this.write(`[EMERGENCY] ${message}`, context, chalk.bgRed.bold)
  }

  alert(message: string, context: object) {
    if (this.logLevel <= 1) {
      return
    }
    this.write(`[ALERT] ${message}`, context, chalk.bgRed)
  }

  critical(message: string, context: object) {
    if (this.logLevel <= 2) {
      return
    }
    this.write(`[CRITICAL] ${message}`, context, chalk.red.bold)
  }

  error(message: string, context: object|null = null) {
    if (this.logLevel <= 3) {
      return
    }
    this.write(`[ERROR] ${message}`, context, chalk.red)
  }

  warning(message: string, context: object|null = null) {
    if (this.logLevel <= 4) {
      return
    }
    this.write(`[WARNING] ${message}`, context, chalk.yellow)
  }

  notice(message: string, context: object|null = null) {
    if (this.logLevel <= 5) {
      return
    }
    this.write(`[NOTICE] ${message}`, context)
  }

  info(message: string, context: object|null = null) {
    if (this.logLevel <= 6) {
      return
    }
    this.write(`[INFO] ${message}`, context)
  }

  debug(message: string, context: object|null = null) {
    if (this.logLevel <= 7) {
      return
    }
    this.write(`[DEBUG] ${message}`, context, chalk.gray)
  }

  log(level: string , message: string, context: object|null = null) {
    this[level](message, context)
  }

  write(message: string, context: object|null = null, wrapper: Function|null = null) {
    let contextText = context ? ("\n" + JSON.stringify(context)) : ''

    if (wrapper) {
      message = wrapper(message + contextText)
    }

    process.stderr.write(message + "\n")
  }
}
