import CommandsPluginInterface from './CommandsPluginInterface'

export function isCommandsPlugin(plugin: Object | CommandsPluginInterface): plugin is CommandsPluginInterface {
  if ("getCommands" in plugin && typeof plugin.getCommands === 'function') {
    return true
  }

  return false
}

export default isCommandsPlugin
