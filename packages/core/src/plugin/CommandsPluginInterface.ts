import { Command } from 'commander'

export default interface CommandsPluginInterface {
  getCommands(): Command[]

  getNamespace(): string
}
