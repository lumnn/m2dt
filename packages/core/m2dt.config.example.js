const https = require('https')
// const { ProductToConfigurable } = require('./build/plugin/product-to-configurable/ProductToConfigurable')

module.exports = {
  m2dt: {
    plugins: [
      // new ProductToConfigurable()
    ]
  },
  magento: {
    profiles: {
      local: {
        isDefault: true,
        api: {
          url: 'https://localhost',
          consumerKey: 'cccc',
          consumerSecret: 'ssss',
          accessToken: 'ttttt',
          tokenSecret: 'tscs',
        },
        // allows self-signed certificates
        axios: {
          httpsAgent: new https.Agent({
            rejectUnauthorized: false
          }),
        }
      },
      other: {
        api: {
          url: 'https://example.org',
          consumerKey: 'cccc',
          consumerSecret: 'ssss',
          accessToken: 'ttttt',
          tokenSecret: 'tscs',
        }
      }
    }
  }
}
