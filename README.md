# Magento 2 Data Tools - CLI for Magento 2

## Installing

1. Clone this repository
2. `npm install`
3. `npm run build`
4. Create `m2dt.config.js` config based on the `packages/core/m2dt.config.example.js`
5. Run `bin/m2dt`

## Development

For development start `npm run dev` and work inside `packages` directory

## Usage

Once you install npm package or clone this repository, you should create config
in a folder where you executing commands.

then either:

`bin/m2dt` - if you cloned repository
`npx m2dt` - if installed from npm

Running command without any arguments will show you all available options.

Each command provide it's help through `--help` argument.

## Config

```js
// m2dt.config.js

const https = require('https')

module.exports = {
  // Plugins can now allow to extend m2dt functionallity, by adding commands
  plugins: [],
  magento: {
    profiles: {
      // you can add as many profiles as you want
      // they'll be referenced by this objects key name
      local: {
        // if no profile is specified during command execution, then the default
        // one, or if missing the first one will be used
        isDefault: true,
        api: {
          url: 'https://localhost',
          consumerKey: 'cccc',
          consumerSecret: 'ssss',
          accessToken: 'ttttt',
          tokenSecret: 'tscs',
        },
        // allows self-signed certificates
        // optional
        axios: {
          httpsAgent: new https.Agent({
            rejectUnauthorized: false
          }),
        }
      },
      other: {
        api: {
          url: 'https://example.org',
          consumerKey: 'cccc',
          consumerSecret: 'ssss',
          accessToken: 'ttttt',
          tokenSecret: 'tscs',
        }
      }
    }
  }
}
```

## Helpers

To work better with JSON output you may want to use following programs:

- [jq](https://stedolan.github.io/jq/)
- [fx](https://www.npmjs.com/package/fx)

## Examples

You'll need [jq](https://stedolan.github.io/jq/) for these examples.

**Get product name, sku and price**

```bash
bin/m2dt get product TEST -r -f name price sku
```

**Get product with much better attribute output**

```bash
bin/m2dt get product TEST -r | jq '
  .custom_attributes = (
    .custom_attributes | map({ (.attribute_code): .value }) | add
  )'
```

**Match attributes by the ones that contain "meta" tag**

```bash
bin/m2dt get product TEST -r -F custom_attributes | jq '
  { custom_attributes: .custom_attributes | map(select(.attribute_code | contains("meta"))) }'
```

**Get product and update few required values for creating new one**

```bash
bin/m2dt get product TEST -r | jq '
    .id = null
  | .sku = .sku + "2"
  | .custom_attributes = (
    .custom_attributes
    | map(if .attribute_code == "merchant_feed_product_id" then { attribute_code, value: (.value + "2") } else . end)
  )'
```

**Get product output with few values changed**

This is really usefull to quickly duplicate product.

```bash
bin/m2dt get product TEST -r | jq '
  del(.id, .extension_attributes.stock_item)
  | .sku = .sku + "2"
  | .custom_attributes = (
    .custom_attributes | map({ (.attribute_code): .value }) | add
    | .url_key = (.url_key + "2")
    | .merchant_feed_product_id = (.merchant_feed_product_id + "2")
    | to_entries | map_values({ attribute_code: .key, value })
  )
  | .media_gallery_entries = (
    .media_gallery_entries | map(. | del
  )
'
```

**Compare 2 products**

You may replace `diff --color` with your diff tool

```bash
diff --color <(bin/m2dt get product TEST -r | jq -S) <(bin/m2dt get product TEST2 -r | jq -S)
```

### Category

**Assign products from one category into another**

`bin/m2dt category:add-products 4851 $(bin/m2dt api get categories/4283/products | jq '.[].sku' -r | tr -s '\n' ' ')`
