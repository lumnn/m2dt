declare module 'magento2-api-wrapper' {
  import { AxiosInstance, AxiosError, AxiosResponse, AxiosRequestConfig } from 'axios'

  interface Magento2ApiConfig {
    url: string;
    consumerKey?: string;
    consumerSecret?: string;
    accessToken?: string;
    tokenSecret?: string;
  }

  interface Magento2ApiConstructorOptions {
    api: Magento2ApiConfig;
    axios?: AxiosRequestConfig;
  }

  interface Magento2ApiAxiosRequestConfig extends AxiosRequestConfig {
    storeCode?: string
  }

  interface Magento2ApiAxiosResponse<T = any> extends AxiosResponse<T> {
    magento: Magento2Api
  }

  export default class Magento2Api {
    constructor (config: Magento2ApiConfig);
    axios: AxiosInstance;
    baseUrl: string;
    getStoreBaseUrl (storeCode: string): string;
    request: any;
    get: any;
    delete: any;
    head: any;
    options: any;
    post: any;
    put: any;
    patch: any;
  }

  interface Magento2ApiError extends AxiosError {
    magento: Magento2Api;
  }
}
